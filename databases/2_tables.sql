/* Table Menus */

CREATE TABLE IF NOT EXISTS `m_menus` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` VARCHAR(100) NOT NULL,
  `link` VARCHAR(255) DEFAULT '#',
  `icon` VARCHAR(100) NULL,
  `class` VARCHAR(100) NULL,
  `parent_id` INT UNSIGNED NULL,
  `status` TINYINT DEFAULT 1,
  PRIMARY KEY `pk_master_menus`(`id`)
) ENGINE = InnoDB;

/* Table Users */

CREATE TABLE IF NOT EXISTS `m_users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100),
  `email` VARCHAR(100),
  `password` VARCHAR(255),
  `bio` VARCHAR(255),
  `status` TINYINT DEFAULT 1,
  `api_key` VARCHAR(255),
  PRIMARY KEY `pk_master_users`(`id`)
) ENGINE = InnoDB;

/* Table Posts */

CREATE TABLE IF NOT EXISTS `m_posts` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `content` TEXT,
  `uri` VARCHAR(255) NOT NULL,
  `status` TINYINT DEFAULT 0,
  PRIMARY KEY `pk_master_posts`(`id`)
) ENGINE = InnoDB;

/* Table Pages */

CREATE TABLE IF NOT EXISTS `m_pages` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `content` TEXT,
  `uri` VARCHAR(255) NOT NULL,
  `status` TINYINT DEFAULT 0,
  PRIMARY KEY `pk_master_pages`(`id`)
) ENGINE = InnoDB;

/* Table Tags */

CREATE TABLE IF NOT EXISTS `m_tags` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` VARCHAR(255) NOT NULL,
  `status` TINYINT DEFAULT 1,
  PRIMARY KEY `pk_master_tags`(`id`)
) ENGINE = InnoDB;

/* Table Medias */

CREATE TABLE IF NOT EXISTS `m_medias` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `uri` VARCHAR(255) NOT NULL,
  `status` TINYINT DEFAULT 1,
  PRIMARY KEY `pk_master_medias`(`id`)
) ENGINE = InnoDB;

/* Table Departments */

CREATE TABLE IF NOT EXISTS `m_departments` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `status` TINYINT DEFAULT 1,
  PRIMARY KEY `pk_master_departments`(`id`)
) ENGINE = InnoDB;

/* Table Roles */

CREATE TABLE IF NOT EXISTS `m_roles` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `status` TINYINT DEFAULT 1,
  PRIMARY KEY `pk_master_roles`(`id`)
) ENGINE = InnoDB;