/* Alter users add field face_id, department_id, role_id */

ALTER TABLE `m_users` ADD COLUMN `face_id` INT UNSIGNED NOT NULL;
ALTER TABLE `m_users` ADD COLUMN `department_id` INT UNSIGNED NOT NULL;
ALTER TABLE `m_users` ADD COLUMN `role_id` INT UNSIGNED NOT NULL;

ALTER TABLE `m_users` ADD INDEX `m_users_face_id` (`face_id` ASC);
ALTER TABLE `m_users` ADD INDEX `m_users_department_id` (`department_id` ASC);
ALTER TABLE `m_users` ADD INDEX `m_users_role_id` (`role_id` ASC);

ALTER TABLE `m_users`
ADD CONSTRAINT `fk_user_face_consttraint`
  FOREIGN KEY (`face_id`)
  REFERENCES `m_medias` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `m_users`
ADD CONSTRAINT `fk_user_department_consttraint`
  FOREIGN KEY (`department_id`)
  REFERENCES `m_departments` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
  
ALTER TABLE `m_users`
ADD CONSTRAINT `fk_user_role_consttraint`
  FOREIGN KEY (`role_id`)
  REFERENCES `m_roles` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

/* Alter posts add field cover_id, user_id */

ALTER TABLE `m_posts` ADD COLUMN `cover_id` INT UNSIGNED NOT NULL;
ALTER TABLE `m_posts` ADD COLUMN `user_id` INT UNSIGNED NOT NULL;

ALTER TABLE `m_posts` ADD INDEX `m_posts_cover_id` (`cover_id` ASC);
ALTER TABLE `m_posts` ADD INDEX `m_posts_user_id` (`user_id` ASC);

ALTER TABLE `m_posts`
ADD CONSTRAINT `fk_post_cover_consttraint`
  FOREIGN KEY (`cover_id`)
  REFERENCES `m_medias` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `m_posts`
ADD CONSTRAINT `fk_post_user_consttraint`
  FOREIGN KEY (`user_id`)
  REFERENCES `m_users` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

/* Alter pages add field cover_id, user_id */

ALTER TABLE `m_pages` ADD COLUMN `cover_id` INT UNSIGNED NULL;
ALTER TABLE `m_pages` ADD COLUMN `user_id` INT UNSIGNED NOT NULL;

ALTER TABLE `m_pages` ADD INDEX `m_pages_cover_id` (`cover_id` ASC);
ALTER TABLE `m_pages` ADD INDEX `m_pages_user_id` (`user_id` ASC);

ALTER TABLE `m_pages`
ADD CONSTRAINT `fk_page_user_consttraint`
  FOREIGN KEY (`user_id`)
  REFERENCES `m_users` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

/* Alter tags add field posts_id, pages_id */

ALTER TABLE `m_tags` ADD COLUMN `posts_id` INT NULL;
ALTER TABLE `m_tags` ADD COLUMN `pages_id` INT NULL;