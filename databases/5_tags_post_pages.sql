/** CREATE TABLE TAGS POST */

CREATE TABLE IF NOT EXISTS `t_tags_posts` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag_id` INT UNSIGNED NOT NULL,
  `post_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY `pk_id`(`id`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `t_tags_pages` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `tag_id` INT UNSIGNED NOT NULL,
  `page_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY `pk_id`(`id`)
) ENGINE = InnoDB;

ALTER TABLE `t_tags_posts`
ADD CONSTRAINT `fk_tags_posts_tag_consttraint`
  FOREIGN KEY (`tag_id`)
  REFERENCES `m_tags` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `t_tags_posts`
ADD CONSTRAINT `fk_tags_posts_post_consttraint`
  FOREIGN KEY (`post_id`)
  REFERENCES `m_posts` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `t_tags_pages`
ADD CONSTRAINT `fk_tags_pages_tag_consttraint`
  FOREIGN KEY (`tag_id`)
  REFERENCES `m_tags` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `t_tags_pages`
ADD CONSTRAINT `fk_tags_pages_post_consttraint`
  FOREIGN KEY (`page_id`)
  REFERENCES `m_pages` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;