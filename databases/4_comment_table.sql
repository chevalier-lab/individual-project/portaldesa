/* Table Comment */

CREATE TABLE IF NOT EXISTS `t_comment` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255),
  `content` TEXT,
  `user_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY `pk_id`(`id`)
) ENGINE = InnoDB;

ALTER TABLE `t_comment`
ADD CONSTRAINT `fk_comment_user_consttraint`
  FOREIGN KEY (`user_id`)
  REFERENCES `m_users` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `t_comment` ADD COLUMN `posts_id` INT NULL;
ALTER TABLE `t_comment` ADD COLUMN `pages_id` INT NULL;