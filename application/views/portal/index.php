<?php
    $this->load->view('template/head.php', array(
        'title' => 'Portal Desa',
        'head_unit' => '
            <link rel="stylesheet" href='.base_url("assets/plugins/fontawesome-free/css/all.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/jqvmap/jqvmap.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/dist/css/adminlte.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/daterangepicker/daterangepicker.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/summernote/summernote-bs4.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css").'>
            <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        '
    ));
?>
<body class="hold-transition login-page">
    <?php
        // Component Heading
        $this->load->view('portal/component/heading', array(
            "heading"   => $page
        ));

        // Call Pages
        $this->load->view($location);
    ?>
</body>
<?php
    $this->load->view('template/foot.php', array(
        'foot_unit' => '
            <script src='.base_url("assets/plugins/jquery/jquery.min.js").'></script>
            <script src='.base_url("assets/plugins/jquery-ui/jquery-ui.min.js").'></script>
            <script src='.base_url("assets/plugins/bootstrap/js/bootstrap.bundle.min.js").'></script>
            <script src='.base_url("assets/plugins/chart.js/Chart.min.js").'></script>
            <script src='.base_url("assets/plugins/sparklines/sparkline.js").'></script>
            <script src='.base_url("assets/plugins/jqvmap/jquery.vmap.min.js").'></script>
            <script src='.base_url("assets/plugins/jqvmap/maps/jquery.vmap.usa.js").'></script>
            <script src='.base_url("assets/plugins/jquery-knob/jquery.knob.min.js").'></script>
            <script src='.base_url("assets/plugins/moment/moment.min.js").'></script>
            <script src='.base_url("assets/plugins/daterangepicker/daterangepicker.js").'></script>
            <script src='.base_url("assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js").'></script>
            <script src='.base_url("assets/plugins/summernote/summernote-bs4.min.js").'></script>
            <script src='.base_url("assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js").'></script>
            <script src='.base_url("assets/plugins/datatables/jquery.dataTables.min.js").'></script>
            <script src='.base_url("assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js").'></script>
            <script src='.base_url("assets/plugins/datatables-responsive/js/dataTables.responsive.min.js").'></script>
            <script src='.base_url("assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js").'></script>
            <script src='.base_url("assets/dist/js/adminlte.js").'></script>
            <script src='.base_url("assets/dist/js/pages/dashboard.js").'></script>
            <script src='.base_url("assets/dist/js/demo.js").'></script>
            <script>
                $.widget.bridge("uibutton", $.ui.button)

                // Sign in
                $("#sign-in").on("click", function() {
                    var request = $("#sign-in-form").serialize()
                    $.post("'.base_url("index.php/controller/portal_action_sign_in").'",request,
                    function(data, status){
                        data = JSON.parse(data)
                        data = data.data
                        if(data.code == 200) 
                            location.assign(data.hotlink)
                        else
                            alert(data.message)
                    });
                })
            </script>
        '
    ));
?>