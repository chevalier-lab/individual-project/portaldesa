<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form class="row" id="form_tags">
            <div class="col-md-3">
                <a href="#" class="btn btn-primary btn-block mb-3" id="save_tag">Simpan</a>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Buat Tags</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <input class="form-control" placeholder="Label" id="label" name="label">
                        </div>
                        <div class="form-group">
                            <select class="form-control select2bs4" id="status" name="status">
                                <option disabled selected>Pilih Status</option>
                                <option value="1">Aktif</option>
                                <option value="'0'">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
