<?php
    $news = array();
    $tags = array();
    if (isset($data["news"])) {
        $news = $data["news"];
    }
    if (isset($data["tags"])) {
        $tags = $data["tags"];
    }
?>
<!-- Main content -->
<section class="content">
    
    <div class="container-fluid">
        <form class="row" id="form_news_update" enctype="multipart/form-data">
            <div class="col-md-3">
                <a href="#" class="btn btn-primary btn-block mb-3" id="save_news_update">Simpan</a>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Tags</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="card-body p-0">
                        <ul class="nav nav-pills flex-column" id="quick_tag_container">
                            <?php
                                if (isset($tags)) {
                                    foreach ($tags as $item) {
                                        echo ('
                                            <li class="nav-item quick_tag_item" onclick="this.parentNode.removeChild(this)">
                                                <a href="#" class="nav-link">'.$item["label"].'</a>
                                            </li>
                                        ');
                                    }
                                }
                            ?>
                        </ul>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <div class="form-group">
                            <input class="form-control" placeholder="Tag" id="quick_tag">
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Cover</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="form-group">
                            <input type="hidden" name="news_id" value="<?= $news["id"]; ?>">
                            <div class="btn btn-default btn-file btn-block">
                                <i class="fas fa-paperclip"></i> Pilih Gambar
                                <input type="file" name="photos" id="photos">
                            </div>
                            <img src="<?= $news['cover_uri'] . $news['cover']; ?>" style="max-width: 100%" id="preview_photos" class="pt-2">
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Edit Berita</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <input class="form-control" placeholder="Judul" id="title" name="title" value="<?= $news["title"] ?>">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control compose-textarea" placeholder="Konten" id="content" name="content"><?= $news["content"] ?></textarea>    
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
