<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
            <!-- Default box -->
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10 col-sm-12">
                            <h3>Selamat datang di portal Desa Netpala</h3>
                            <p>Kami telah mengumpulkan beberapa tautan untuk membantu Anda memulai:</p>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <a href="<?= base_url('index.php/controller/createNews'); ?>" class="btn btn-primary btn-lg btn-block text-white">
                                <i class="fas fa-plus"></i> Buat Berita
                            </a>
                        </div>
                    </div>
                    <div class="row" id="info-box">
                        <div class="col-sm-12">
                            <?php
                                $this->load->view('dashboard/component/infobox', array(
                                    "title" => "Informasi untuk anda:",
                                    "items" => array(
                                        array(
                                            "icon"      => "fa-newspaper",
                                            "text"      => "Total Berita",
                                            "bgColor"   => "bg-info",
                                            "id"        => "news-counter"
                                        ),
                                        array(
                                            "icon"      => "fa-file",
                                            "text"      => "Total Halaman",
                                            "bgColor"   => "bg-success",
                                            "id"        => "pages-counter"
                                        ),
                                        array(
                                            "icon"      => "fa-users",
                                            "text"      => "Total Pengguna",
                                            "bgColor"   => "bg-warning",
                                            "id"        => "users-counter"
                                        ),
                                        array(
                                            "icon"      => "fa-comments",
                                            "text"      => "Total Komentar",
                                            "bgColor"   => "bg-danger",
                                            "id"        => "comments-counter"
                                        )
                                    )
                                ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->