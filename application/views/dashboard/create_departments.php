<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form class="row" id="form_department" enctype="multipart/form-data">
            <div class="col-md-3">
                <a href="#" class="btn btn-primary btn-block mb-3" id="save_department">Simpan</a>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Buat Jabatan</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <input class="form-control" placeholder="Nama Jabatan" id="name" name="name">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->