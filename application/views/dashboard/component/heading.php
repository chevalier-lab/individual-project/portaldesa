<!-- HEADING -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1><?= $heading; ?></h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <?php
                        foreach ($indexing as $item) {
                            if ($item["active"]) {
                                echo ('
                                    <li class="breadcrumb-item active">
                                        '.$item["text"].'
                                    </li>
                                ');
                            }
                            else {
                                echo ('
                                    <li class="breadcrumb-item">
                                        <a href="'.$item["link"].'">
                                        '.$item["text"].'
                                        </a>
                                    </li>
                                ');
                            }
                        }
                    ?>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- HEADING -->