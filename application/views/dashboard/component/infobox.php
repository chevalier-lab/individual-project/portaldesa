<h3>Informasi untuk anda:</h3>
<div class="row">
    <?php
        foreach ($items as $item) {
            echo ('
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box">
                        <span class="info-box-icon '.$item["bgColor"].'"><i class="fas '.$item["icon"].'"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">'.$item["text"].'</span>
                            <span class="info-box-number" id="' . $item["id"] .'"></span>
                        </div>
                    </div>
                </div>
            ');
        }
    ?>
</div>