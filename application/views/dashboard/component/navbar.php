<!-- Navbar -->
<?php 
    $this->load->view ("dashboard/helper/navbar.php");
?>
<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
            <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <?php
                if (count($items["left"]) > 0) {
                    foreach ($items["left"] as $item) {
                        if (count($item["child"]) == 0) {
                            renderAndcheckHaveIconOrNot($item);
                        }
                        else {
                            renderWithChild($item);
                        }
                    }
                }
            ?>
        </ul>

        <!-- SEARCH FORM -->
        <?php
            if ($items["search"]) {
                renderSearch();
            }
        ?>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <?php
                if (count($items["right"]) > 0) {
                    foreach ($items["right"] as $item) {
                        if (count($item["child"]) == 0) {
                            renderAndcheckHaveIconOrNot($item);
                        }
                        else {
                            renderWithChild($item);
                        }
                    }
                }
            ?>
        </ul>
    </div>
</nav>
<!-- /.navbar -->