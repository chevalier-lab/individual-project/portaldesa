<?php
    $tag = array();
    if (isset($data["tag"])) {
        $tag = $data["tag"];
    }
?>
<!-- Main content -->
<section class="content">

    <div class="container-fluid">
        <form class="row" id="form_tag_update" enctype="multipart/form-data">
            <div class="col-md-3">
                <a href="#" class="btn btn-primary btn-block mb-3" id="save_tag_update">Simpan</a>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Edit Tags</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <input class="form-control" placeholder="Label" id="label" name="label" value="<?= $tag["label"] ?>">
                            <input class="form-control" id="tag_id" name="tag_id" value="<?= $tag["id"] ?>" type="hidden">
                        </div>
                        <div class="form-group">
                            <select class="form-control select2bs4" id="status" name="status">
                                <option disabled selected>Pilih Status</option>
                                <?php
                                if ($tag["status"] == 1) {
                                    echo ('<option value="1" selected>Aktif</option>');
                                    echo ('<option value="`0`">Tidak Aktif</option>');
                                } else {
                                    echo ('<option value="`0`" selected>Tidak Aktif</option>');
                                    echo ('<option value="1">Aktif</option>');
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->
