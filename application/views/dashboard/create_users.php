<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form class="row" id="form_user" enctype="multipart/form-data">
            <div class="col-md-3">
                <a href="#" class="btn btn-primary btn-block mb-3" id="save_user">Simpan</a>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Foto Profil</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="card-body p-0">

                    </div>

                    <div class="card-footer">
                        <div class="form-group">
                            <!-- <div class="btn btn-default btn-file btn-block">
                                <i class="fas fa-paperclip"></i> Pas Foto -->
                            <input type="file" name="photos" id="photos">
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Buat Pengguna</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <input class="form-control" placeholder="Nama Lengkap" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Email" id="email" name="email">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <select class="form-control select2bs4" id="department" name="department">
                                <option disabled selected>Pilih Jabatan</option>
                                <?php
                                if (isset($data)) {
                                    foreach ($data["departments"] as $item) {
                                        echo ('<option value="' . $item["id"] . '">' . $item["name"] . '</option>');
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control select2bs4" id="role" name="role">
                                <option disabled selected>Hak Akses</option>
                                <?php
                                if (isset($data)) {
                                    foreach ($data["roles"] as $item) {
                                        echo ('<option value="' . $item["id"] . '">' . $item["name"] . '</option>');
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control compose-textarea" placeholder="Tentang Saya" id="bio" name="bio"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->