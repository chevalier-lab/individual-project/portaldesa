<?php
$this->load->view('template/head.php', array(
    'title' => 'Portal Desa',
    'head_unit' => '
            <link rel="stylesheet" href=' . base_url("assets/plugins/fontawesome-free/css/all.min.css") . '>
            <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
            <link rel="stylesheet" href=' . base_url("assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css") . '>
            <link rel="stylesheet" href=' . base_url("assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css") . '>
            <link rel="stylesheet" href=' . base_url("assets/plugins/jqvmap/jqvmap.min.css") . '>
            <link rel="stylesheet" href=' . base_url("assets/dist/css/adminlte.min.css") . '>
            <link rel="stylesheet" href=' . base_url("assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css") . '>
            <link rel="stylesheet" href=' . base_url("assets/plugins/daterangepicker/daterangepicker.css") . '>
            <link rel="stylesheet" href=' . base_url("assets/plugins/select2/css/select2.min.css") . '>
            <link rel="stylesheet" href=' . base_url("assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css") . '>
            <link rel="stylesheet" href=' . base_url("assets/plugins/summernote/summernote-bs4.css") . '>
            <link rel="stylesheet" href=' . base_url("assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css") . '>
            <link rel="stylesheet" href=' . base_url("assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css") . '>
            <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        '
));
?>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <?php
        // Component Navbar
        $this->load->view('dashboard/component/navbar', array(
            "logo"      => base_url("assets/dist/img/logo.png"),
            "title"     => "Desa Netpala",
            "items"     => array(
                "left"      => array(),
                "search"    => true,
                "right"     => array(
                    array(
                        "id"    => "",
                        "link"  => base_url('index.php/controller/portal_action_sign_out'),
                        "text"  => "Keluar",
                        "class" => "btn btn-danger text-white",
                        "icon"  => "fa-sign-out-alt",
                        "child" => array()
                    )
                )
            )
        ));

        // Component Sidebar
        $this->load->view('dashboard/component/sidebar', array(
            "logo"      => base_url("assets/dist/img/logo.png"),
            "title"     => "POS Apps",
            "user"      => array(
                "name"  => "Administrator",
                "face"  => base_url("assets/dist/img/avatar.png")
            ),
            "items"     => array(
                array(
                    "id"    => "",
                    "link"  => base_url("index.php/controller/dashboard"),
                    "text"  => "Dashboard",
                    "class" => "",
                    "icon"  => "fa-tachometer-alt",
                    "child" => array()
                ),
                array(
                    "id"    => "",
                    "link"  => "#",
                    "text"  => "Berita",
                    "class" => "",
                    "icon"  => "fa-newspaper",
                    "child" => array(
                        array(
                            "id"    => "",
                            "link"  => base_url("index.php/controller/createNews"),
                            "text"  => "Buat berita",
                            "class" => "",
                            "icon"  => ""
                        ),
                        array(
                            "id"    => "",
                            "link"  => base_url("index.php/controller/listNews"),
                            "text"  => "Semua berita",
                            "class" => "",
                            "icon"  => ""
                        )
                    )
                ),
                array(
                    "id"    => "",
                    "link"  => "#",
                    "text"  => "Halaman",
                    "class" => "",
                    "icon"  => "fa-file",
                    "child" => array(
                        array(
                            "id"    => "",
                            "link"  => base_url("index.php/controller/createPages"),
                            "text"  => "Buat halaman",
                            "class" => "",
                            "icon"  => ""
                        ),
                        array(
                            "id"    => "",
                            "link"  => base_url("index.php/controller/listPages"),
                            "text"  => "Semua halaman",
                            "class" => "",
                            "icon"  => ""
                        )
                    )
                ),
                array(
                    "id"    => "",
                    "link"  => base_url("index.php/controller/menus"),
                    "text"  => "Menu",
                    "class" => "",
                    "icon"  => "fa-bars",
                    "child" => array()
                ),
                array(
                    "id"    => "",
                    "link"  => base_url("index.php/controller/users"),
                    "text"  => "Pengguna",
                    "class" => "",
                    "icon"  => "fa-users",
                    "child" => array()
                ),
                array(
                    "id"    => "",
                    "link"  => "#",
                    "text"  => "Master Data",
                    "class" => "",
                    "icon"  => "fa-database",
                    "child" => array(
                        array(
                            "id"    => "",
                            "link"  => base_url("index.php/controller/list_departments"),
                            "text"  => "Jabatan",
                            "class" => "",
                            "icon"  => ""
                        ),
                        array(
                            "id"    => "",
                            "link"  => base_url("index.php/controller/list_tags"),
                            "text"  => "Tags",
                            "class" => "",
                            "icon"  => ""
                        )
                    )
                )
            )
        ));
        ?>
        <div class="content-wrapper">
            <?php
            // Component Heading
            $this->load->view('dashboard/component/heading', array(
                "heading"   => $page,
                "indexing"  => $indexing
            ));

            // Notification Area
            echo ('
                <div class="container-fluid">
                    <div class="col-sm-12" id="notificationArea"></div>
                </div>
            ');

            // Call Pages
            $this->load->view($location);
            ?>
        </div>
    </div>
</body>
<?php
$this->load->view('template/foot.php', array(
    'foot_unit' => '
            <script src=' . base_url("assets/plugins/jquery/jquery.min.js") . '></script>
            <script src=' . base_url("assets/plugins/jquery-ui/jquery-ui.min.js") . '></script>
            <script src=' . base_url("assets/plugins/bootstrap/js/bootstrap.bundle.min.js") . '></script>
            <script src=' . base_url("assets/plugins/chart.js/Chart.min.js") . '></script>
            <script src=' . base_url("assets/plugins/sparklines/sparkline.js") . '></script>
            <script src=' . base_url("assets/plugins/jqvmap/jquery.vmap.min.js") . '></script>
            <script src=' . base_url("assets/plugins/jqvmap/maps/jquery.vmap.usa.js") . '></script>
            <script src=' . base_url("assets/plugins/jquery-knob/jquery.knob.min.js") . '></script>
            <script src=' . base_url("assets/plugins/moment/moment.min.js") . '></script>
            <script src=' . base_url("assets/plugins/daterangepicker/daterangepicker.js") . '></script>
            <script src=' . base_url("assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js") . '></script>
            <script src=' . base_url("assets/plugins/summernote/summernote-bs4.min.js") . '></script>
            <script src=' . base_url("assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js") . '></script>
            <script src=' . base_url("assets/plugins/datatables/jquery.dataTables.min.js") . '></script>
            <script src=' . base_url("assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js") . '></script>
            <script src=' . base_url("assets/plugins/datatables-responsive/js/dataTables.responsive.min.js") . '></script>
            <script src=' . base_url("assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js") . '></script>
            <script src=' . base_url("assets/plugins/select2/js/select2.full.min.js") . '></script>
            <script src=' . base_url("assets/dist/js/adminlte.js") . '></script>
            <script src=' . base_url("assets/dist/js/pages/dashboard.js") . '></script>
            <script src=' . base_url("assets/dist/js/demo.js") . '></script>
            <script>
                $.widget.bridge("uibutton", $.ui.button)
                $(function () {
                    // Add text editor
                    $(".compose-textarea").summernote()

                    // Add Select2
                    $(".select2bs4").select2({
                        theme: "bootstrap4"
                    })

                    // Set Handler For Photos Clicked
                    $("#photos").on("change", function() {
                        if (this.files.length > 0) {
                            const img = document.getElementById("preview_photos");
                            img.src = URL.createObjectURL(this.files[0]);
                            img.onload = function() {
                                URL.revokeObjectURL(this.src);
                            }
                        }
                    });

                    // Set Handler For Quick Tags
                    $("#quick_tag").on("keydown", function(e) {
                        if (e.which == 13) {
                            e.preventDefault();
                            createQuickTag(this.value)
                        }
                    });
                })

                /** UTILITY ===================================================================================== **/
                // Create Tag Quickly
                function createQuickTag(value) {
                    $("#quick_tag_container").append(`
                        <li class="nav-item quick_tag_item" onclick="this.parentNode.removeChild(this)">
                            <a href="#" class="nav-link">${value}</a>
                        </li>
                    `);
                    $("#quick_tag").val("");   
                }

                // Get Tag Quickly
                function getQuickTag() {
                    var tags = document.getElementsByClassName("quick_tag_item");
                    var temp = []
                    for (var i = 0; i < tags.length; i++) {
                        temp.push({
                            item: tags[i].children[0].textContent
                        })
                    }
                    return JSON.stringify({
                        tags: temp
                    });
                }

                /** ALL CREATES ===================================================================================== **/
                // Create User
                $("#form_user").ready(function() {
                    $("#save_user").on("click", function() {
                        var formData = new FormData($("#form_user")[0]);
                        $.ajax({
                            url: "' . base_url("index.php/controller/users_action_create") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil menambahkan pengguna</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0); 
                                    $("#form_user").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/users") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal menambahkan pengguna</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });

                // Create Department
                $("#form_department").ready(function() {
                    $("#save_department").on("click", function() {
                        var formData = new FormData($("#form_department")[0]);
                        $.ajax({
                            url: "' . base_url("index.php/controller/department_action_create") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil menambahkan jabatan</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0);
                                    $("#form_department").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/list_departments") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal menambahkan jabatan</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });

                // Create News
                $("#form_news").ready(function() {
                    $("#save_news").on("click", function() {
                        var formData = new FormData($("#form_news")[0]);
                        formData.append("tags", getQuickTag());
                        $.ajax({
                            url: "' . base_url("index.php/controller/news_action_create") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil menambahkan berita</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0);
                                    $("#form_news").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/listNews") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal menambahkan berita</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });

                // Create Page
                $("#form_page").ready(function() {
                    $("#save_page").on("click", function() {
                        var formData = new FormData($("#form_page")[0]);
                        formData.append("tags", getQuickTag());
                        $.ajax({
                            url: "' . base_url("index.php/controller/pages_action_create") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil menambahkan halaman</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0);
                                    $("#form_page").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/listPages") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal menambahkan halaman</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });

                // Create Menu
                $("#form_menus").ready(function() {
                    $("#save_menu").on("click", function() {
                        var formData = new FormData($("#form_menus")[0]);
                        $.ajax({
                            url: "' . base_url("index.php/controller/menus_action_create") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil membuat menu</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0); 
                                    $("#form_user").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/menus") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal membuat menu</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });

                // Create Tag
                $("#form_tags").ready(function() {
                    $("#save_tag").on("click", function() {
                        var formData = new FormData($("#form_tags")[0]);
                        $.ajax({
                            url: "' . base_url("index.php/controller/tag_action_create") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil membuat tag</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0); 
                                    $("#form_user").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/list_tags") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal membuat tag</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });

                /** ALL REMOVES ===================================================================================== **/
                // Remove User
                function remove_user(id) {
                    $.ajax({
                        url: "' . base_url("index.php/controller/users_action_delete") . '?user_id=" + id,
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response);
                            var data = JSON.parse(response);
                            data = data.data;
                            if (data.code == 200) {
                                $("#notificationArea").html(`
                                    <div class="alert alert-success alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Berhasil menghapus pengguna</h5>
                                        ${data.message}
                                    </div>
                                `);
                                $(window).scrollTop(0); 
                                setTimeout(function(){
                                    location.assign("' . base_url("index.php/controller/users") . '");
                                }, 3000);
                            }
                            else {
                                $(window).scrollTop(0); 
                                $("#notificationArea").html(`
                                    <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Gagal menghapus pengguna</h5>
                                        ${data.message}
                                    </div>
                                `);
                            }
                        }
                    });
                }

                // Remove Department
                function remove_department(id) {
                    $.ajax({
                        url: "' . base_url("index.php/controller/department_action_delete") . '?department_id=" + id,
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response);
                            var data = JSON.parse(response);
                            data = data.data;
                            if (data.code == 200) {
                                $("#notificationArea").html(`
                                    <div class="alert alert-success alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Berhasil menghapus jabatan</h5>
                                        ${data.message}
                                    </div>
                                `);
                                $(window).scrollTop(0); 
                                setTimeout(function(){
                                    location.assign("' . base_url("index.php/controller/list_departments") . '");
                                }, 3000);
                            }
                            else {
                                $(window).scrollTop(0); 
                                $("#notificationArea").html(`
                                    <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Gagal menghapus jabatan</h5>
                                        ${data.message}
                                    </div>
                                `);
                            }
                        }
                    });
                }

                // Remove News
                function remove_news(id) {
                    $.ajax({
                        url: "' . base_url("index.php/controller/news_action_delete") . '?news_id=" + id,
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response);
                            var data = JSON.parse(response);
                            data = data.data;
                            if (data.code == 200) {
                                $("#notificationArea").html(`
                                    <div class="alert alert-success alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Berhasil menghapus berita</h5>
                                        ${data.message}
                                    </div>
                                `);
                                $(window).scrollTop(0); 
                                setTimeout(function(){
                                    location.assign("' . base_url("index.php/controller/listNews") . '");
                                }, 3000);
                            }
                            else {
                                $(window).scrollTop(0); 
                                $("#notificationArea").html(`
                                    <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Gagal menghapus berita</h5>
                                        ${data.message}
                                    </div>
                                `);
                            }
                        }
                    });
                }

                // Remove Page
                function remove_page(id) {
                    $.ajax({
                        url: "' . base_url("index.php/controller/pages_action_delete") . '?page_id=" + id,
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response);
                            var data = JSON.parse(response);
                            data = data.data;
                            if (data.code == 200) {
                                $("#notificationArea").html(`
                                    <div class="alert alert-success alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Berhasil menghapus halaman</h5>
                                        ${data.message}
                                    </div>
                                `);
                                $(window).scrollTop(0); 
                                setTimeout(function(){
                                    location.assign("' . base_url("index.php/controller/listPages") . '");
                                }, 3000);
                            }
                            else {
                                $(window).scrollTop(0); 
                                $("#notificationArea").html(`
                                    <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Gagal menghapus halaman</h5>
                                        ${data.message}
                                    </div>
                                `);
                            }
                        }
                    });
                }

                // Remove menu
                function remove_menu(id) {
                    $.ajax({
                        url: "' . base_url("index.php/controller/menus_action_delete") . '?menu_id=" + id,
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response);
                            var data = JSON.parse(response);
                            data = data.data;
                            if (data.code == 200) {
                                $("#notificationArea").html(`
                                    <div class="alert alert-success alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Berhasil menghapus menu</h5>
                                        ${data.message}
                                    </div>
                                `);
                                $(window).scrollTop(0); 
                                setTimeout(function(){
                                    location.assign("' . base_url("index.php/controller/menus") . '");
                                }, 3000);
                            }
                            else {
                                $(window).scrollTop(0); 
                                $("#notificationArea").html(`
                                    <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Gagal menghapus menu</h5>
                                        ${data.message}
                                    </div>
                                `);
                            }
                        }
                    });
                }

                // Remove tag
                function remove_tag(id) {
                    $.ajax({
                        url: "' . base_url("index.php/controller/tag_action_delete") . '?tag_id=" + id,
                        data: null,
                        type: "GET",
                        contentType: false,
                        processData: false,
                        success: function(response) {
                            console.log(response);
                            var data = JSON.parse(response);
                            data = data.data;
                            if (data.code == 200) {
                                $("#notificationArea").html(`
                                    <div class="alert alert-success alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Berhasil menghapus tag</h5>
                                        ${data.message}
                                    </div>
                                `);
                                $(window).scrollTop(0); 
                                setTimeout(function(){
                                    location.assign("' . base_url("index.php/controller/list_tags") . '");
                                }, 3000);
                            }
                            else {
                                $(window).scrollTop(0); 
                                $("#notificationArea").html(`
                                    <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <h5>Gagal menghapus tag</h5>
                                        ${data.message}
                                    </div>
                                `);
                            }
                        }
                    });
                }

                /** ALL UPDATES ===================================================================================== **/
                // Update User
                function goto_update_user(id) {
                    location.assign("' . base_url("index.php/controller/usersUpdate?user_id=") . '" + id);
                }
                $("#form_user_update").ready(function() {
                    $("#save_user_update").on("click", function() {
                        var formData = new FormData($("#form_user_update")[0]);
                        $.ajax({
                            url: "' . base_url("index.php/controller/users_action_update") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil memperbaharui pengguna</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0); 
                                    $("#form_user_update").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/users") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal memperbaharui pengguna</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });

                // Update News
                function goto_update_news(id) {
                    location.assign("' . base_url("index.php/controller/updateNews?news_id=") . '" + id);
                }
                $("#form_news_update").ready(function() {
                    $("#save_news_update").on("click", function() {
                        var formData = new FormData($("#form_news_update")[0]);
                        formData.append("tags", getQuickTag());
                        $.ajax({
                            url: "' . base_url("index.php/controller/news_action_update") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil memperbaharui berita</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0); 
                                    $("#form_news_update").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/listNews") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal memperbaharui berita</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });

                // Update Pages
                function goto_update_page(id) {
                    location.assign("' . base_url("index.php/controller/updatePages?page_id=") . '" + id);
                }
                $("#form_pages_update").ready(function() {
                    $("#save_pages_update").on("click", function() {
                        var formData = new FormData($("#form_pages_update")[0]);
                        formData.append("tags", getQuickTag());
                        $.ajax({
                            url: "' . base_url("index.php/controller/pages_action_update") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil memperbaharui halaman</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0); 
                                    $("#form_pages_update").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/listPages") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal memperbaharui halaman</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });

                // Update Menu
                function goto_update_menu(id) {
                    location.assign("' . base_url("index.php/controller/updateMenus?menu_id=") . '" + id);
                }
                $("#form_menu_update").ready(function() {
                    $("#save_menu_update").on("click", function() {
                        var formData = new FormData($("#form_menu_update")[0]);
                        $.ajax({
                            url: "' . base_url("index.php/controller/menus_action_update") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil memperbaharui menu</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0); 
                                    $("#form_user_update").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/menus") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal memperbaharui menu</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });

                // Update Master Data Jabatan
                function goto_update_department(id) {
                    location.assign("' . base_url("index.php/controller/departmentsUpdate?department_id=") . '" + id);
                }
                $("#form_department_update").ready(function() {
                    $("#save_department_update").on("click", function() {
                        var formData = new FormData($("#form_department_update")[0]);
                        $.ajax({
                            url: "' . base_url("index.php/controller/department_action_update") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil memperbaharui jabatan</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0); 
                                    $("#form_department_update").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/list_departments") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal memperbaharui jabatan</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });

                // Update Tag
                function goto_update_tag(id) {
                    location.assign("' . base_url("index.php/controller/update_tags?tag_id=") . '" + id);
                }
                $("#form_tag_update").ready(function() {
                    $("#save_tag_update").on("click", function() {
                        var formData = new FormData($("#form_tag_update")[0]);
                        $.ajax({
                            url: "' . base_url("index.php/controller/tag_action_update") . '",
                            data: formData,
                            type: "POST",
                            contentType: false,
                            processData: false,
                            success: function(response) {
                                console.log(response);
                                var data = JSON.parse(response);
                                data = data.data;
                                if (data.code == 200) {
                                    $("#notificationArea").html(`
                                        <div class="alert alert-success alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Berhasil memperbaharui tag</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                    $(window).scrollTop(0); 
                                    $("#form_user_update").trigger("reset");
                                    setTimeout(function(){
                                        location.assign("' . base_url("index.php/controller/list_tags") . '");
                                    }, 3000);
                                }
                                else {
                                    $(window).scrollTop(0); 
                                    $("#notificationArea").html(`
                                        <div class="alert alert-danger alert-dismissible" style="width: 100%">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <h5>Gagal memperbaharui tag</h5>
                                            ${data.message}
                                        </div>
                                    `);
                                }
                            }
                        });
                    });
                });
                
                /** ALL READS ======================================================================================= **/
                // Dashboard Count
                $("#info-box").ready(function() {
                    $.post("' . base_url("index.php/controller/counter_dashboard") . '",
                    function(data, status){
                        data = JSON.parse(data)
                        data = data.data
                        if(data.code == 200){
                            $("#news-counter").text(data.data.posts_count)
                            $("#pages-counter").text(data.data.pages_count)
                            $("#users-counter").text(data.data.users_count)
                            $("#comments-counter").text(data.data.comments_count)
                        }
                    });
                });

                // Read Menus
                $("#table-menus").ready(function() {
                    $("#table-menus").DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "responsive": true,
                        "processing": true,
                        "ajax": {
                            "url": "' . base_url("index.php/controller/menus_action_read_all") . '",
                            "dataSrc": "data.data"
                        },
                        columns: [
                            {"data": "label"},
                            {
                                "data": null,
                                "render": function (data) {
                                    var link = data.link
                                    var label = data.label
                                    console.log(label)
                                    return `<a href="${link}">${label}</a>`
                                }
                            },
                            {
                                "data": null,
                                "render": function (data) {
                                    if(data.parent_id == null ) return "#";
                                    else return data.parent_label;
                                }
                            },
                            {
                                "data": null,
                                "render": function(data) {
                                    var menu_id = data.menu_id
                                    return `<button type="button"class="btn btn-success btn-sm" onclick="goto_update_menu(this.value)" value="${menu_id}"><i class="fas fa-fw fa-edit"></i></button> 
                                    | 
                                    <button type="button"class="btn btn-danger btn-sm" onClick="remove_menu(this.value)" value="${menu_id}"><i class="fas fa-fw fa-trash"></i></button>`
                                }
                            }
                        ]
                    });
                });

                // Read Users
                $("#table-users").ready(function() {
                    $("#table-users").DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "responsive": true,
                        "processing": true,
                        "ajax": {
                            "url": "' . base_url("index.php/controller/users_action_read_all") . '",
                            "dataSrc": "data.data"
                        },
                        "columns": [
                            {"data": "name"},
                            {"data": "email"},
                            {
                                "data": null,
                                "render": function (data) {
                                    if(data.status == 1 ) return `<span class="badge bg-primary">Aktif</span>`;
                                    else return `<span class="badge bg-warning">Pending</span>`;
                                }
                            },
                            {"data": "role"},
                            {"data": "department"},
                            {
                                "data": null,
                                "render": function(data) {
                                    var id = data.id
                                    return `
                                    <button type="button" class="btn btn-success btn-sm" onclick="goto_update_user(this.value)" value="${id}"><i class="fas fa-fw fa-edit"></i></button> 
                                    | 
                                    <button type="button" class="btn btn-danger btn-sm" onclick="remove_user(this.value)" value="${id}"><i class="fas fa-fw fa-trash"></i></button>
                                    `
                                }
                            },
                        ],
                    });
                });

                // Read Pages
                $("#table-pages").ready(function() {
                    $("#table-pages").DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "responsive": true,
                        "processing": true,
                        "ajax": {
                            "url": "' . base_url("index.php/controller/pages_action_read_all") . '",
                            "dataSrc": "data.data"
                        },
                        columns: [
                            {"data": "title"},
                            {
                                "data": null,
                                "render": function (data) {
                                    var uri = data.uri
                                    return `<a href="pages?uri=${uri}">${uri}</a>`
                                }
                            },
                            {
                                "data": null,
                                "render": function (data) {
                                    if(data.status == 1 ) return `<span class="badge bg-primary">Aktif</span>`;
                                    else return `<span class="badge bg-warning">Pending</span>`;
                                }
                            },
                            {
                                "data": null,
                                "render": function(data) {
                                    console.log(data)
                                    var id = data.page_id
                                    return `
                                    <button type="button" class="btn btn-success btn-sm" onclick="goto_update_page(this.value)" value="${id}"><i class="fas fa-fw fa-edit"></i></button> 
                                    | 
                                    <button type="button" class="btn btn-danger btn-sm" onclick="remove_page(this.value)" value="${id}"><i class="fas fa-fw fa-trash"></i></button>
                                    `
                                }
                            }
                        ]
                    });
                });

                // Read Tags
                $("#table-tags").ready(function() {
                    $("#table-tags").DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "responsive": true,
                        "processing": true,
                        "ajax": {
                            "url": "' . base_url("index.php/controller/tag_action_read_all") . '",
                            "dataSrc": "data.data"
                        },
                        columns: [
                            {"data": "label"},
                            {
                                "data": null,
                                "render": function (data) {
                                    if(data.status == 1 ) return "Aktif";
                                    else return "Tidak Aktif";
                                }
                            },
                            {
                                "data": null,
                                "render": function(data) {
                                    var tag_id = data.tag_id
                                    return `<button type="button"class="btn btn-success btn-sm" onclick="goto_update_tag(this.value)" value="${tag_id}"><i class="fas fa-fw fa-edit"></i></button> 
                                    | 
                                    <button type="button"class="btn btn-danger btn-sm" onclick="remove_tag(this.value)" value="${tag_id}"><i class="fas fa-fw fa-trash"></i></button>`
                                }
                            }
                        ]
                    });
                });

                //Read News
                $("#table-news").ready(function() {
                    $("#table-news").DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "responsive": true,
                        "processing": true,
                        "ajax": {
                            "url": "' . base_url("index.php/controller/news_action_read_all") . '",
                            "dataSrc": "data.data"
                        },
                        columns: [
                            {"data": "title"},
                            {
                                "data": null,
                                "render": function (data) {
                                    var uri = data.uri
                                    return `<a href="${uri}">${uri}</a>`
                                }
                            },
                            {
                                "data": null,
                                "render": function (data) {
                                    if(data.status == 1 ) return `<span class="badge bg-primary">Aktif</span>`;
                                    else return `<span class="badge bg-warning">Pending</span>`;
                                }
                            },
                            {
                                "data": null,
                                "render": function(data) {
                                    var id = data.id
                                    return `
                                    <button type="button" class="btn btn-success btn-sm" onclick="goto_update_news(this.value)" value="${id}"><i class="fas fa-fw fa-edit"></i></button> 
                                    | 
                                    <button type="button" class="btn btn-danger btn-sm" onclick="remove_news(this.value)" value="${id}"><i class="fas fa-fw fa-trash"></i></button>
                                    `
                                }
                            }
                        ]
                    });
                });

                //Read Master Data Departments
                $("#table-departments").ready(function() {
                    $("#table-departments").DataTable({
                        "paging": true,
                        "lengthChange": true,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "responsive": true,
                        "processing": true,
                        "ajax": {
                            "url": "' . base_url("index.php/controller/department_action_read_all") . '",
                            "dataSrc": "data.data"
                        },
                        "columns": [
                            {"data": "name"},
                            {
                                "data": null,
                                "render": function (data) {
                                    if(data.status == 1 ) return `<span class="badge bg-primary">Aktif</span>`;
                                    else return `<span class="badge bg-warning">Pending</span>`;
                                }
                            },
                            {
                                "data": null,
                                "render": function(data) {
                                    var id = data.department_id
                                    return `
                                    <button type="button" class="btn btn-success btn-sm" onclick="goto_update_department(this.value)" value="${id}"><i class="fas fa-fw fa-edit"></i></button> 
                                    | 
                                    <button type="button" class="btn btn-danger btn-sm" onclick="remove_department(this.value)" value="${id}"><i class="fas fa-fw fa-trash"></i></button>
                                    `
                                }
                            }
                        ]
                    });
                });
            </script>
        '
));
?>