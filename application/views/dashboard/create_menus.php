<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <form class="row" id="form_menus">
            <div class="col-md-3">
                <a href="#" class="btn btn-primary btn-block mb-3" id="save_menu">Simpan</a>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Buat Menu</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <input class="form-control" placeholder="Label" id="label" name="label">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Link, Ex: https://www.facebook.com" id="link" name="link">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Icon, Ex: fa fa-home" id="icon" name="icon">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Class, Ex: btn btn-primary" id="class" name="class">
                        </div>
                        <div class="form-group">
                            <select class="form-control select2bs4" id="parent_id" name="parent_id">
                                <option disabled selected>Pilih Parent</option>
                                <?php
                                    if (isset($data)) {
                                        foreach ($data["parent"] as $item) {
                                            echo ('<option value="' . $item["id"] . '">' . $item["label"] . '</option>');
                                        }
                                    }    
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->