<?php
    function renderWithChildSidebar($item) {
        if (count($item["child"]) == 0) {
            echo ('
                <li class="nav-item">
                    <a href="'.$item["link"].'" class="nav-link">
                        <i class="nav-icon fas '.$item["icon"].'"></i>
                        <p>
                            '.$item["text"].'
                        </p>
                    </a>
                </li>
            ');
        }
        else {
            echo ('
                <li class="nav-item has-treeview">
                    <a href="'.$item["link"].'" class="nav-link">
                        <i class="nav-icon fas '.$item["icon"].'"></i>
                        <p>
                            '.$item["text"].'
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
            ');
            foreach ($item["child"] as $child) {
                echo ('
                    <li class="nav-item">
                        <a href="'.$child["link"].'" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>'.$child["text"].'</p>
                        </a>
                    </li>
                ');
            }
            echo ('
                    </ul>
                </li>
            ');
        }
    }
?>