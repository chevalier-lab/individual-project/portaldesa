<?php
    $user = array();
    if (isset($data["user"])) {
        $user = $data["user"];
    }
?>
<!-- Main content -->
<section class="content">
    
    <div class="container-fluid">
        <form class="row" id="form_user_update" enctype="multipart/form-data">
            <div class="col-md-3">
                <a href="#" class="btn btn-primary btn-block mb-3" id="save_user_update">Simpan</a>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Foto Profil</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" src="<?= $user['face_uri'] . $user['face']; ?>" alt="User profile picture">
                        </div>
                    </div>

                    <div class="card-footer">
                        <div class="form-group">
                            <!-- <div class="btn btn-default btn-file btn-block">
                                <i class="fas fa-paperclip"></i> Pas Foto -->
                                <input type="hidden" name="user_id" name="user_id" value="<?= $user["user_id"]; ?>">
                                <input type="file" name="photos" id="photos">
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h3 class="card-title">Edit Pengguna</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="form-group">
                            <input class="form-control" placeholder="Nama Lengkap" id="name" name="name" value="<?= $user["name"] ?>">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Email" id="email" name="email" value="<?= $user["email"] ?>">
                        </div>
                        <div class="form-group">
                            <select class="form-control select2bs4" id="department" name="department">
                                <option disabled>Pilih Jabatan</option>
                                <?php
                                    if (isset($data)) {
                                        foreach ($data["departments"] as $item) {
                                            if ($item["id"] == $user["department_id"])
                                            echo ('<option value="'.$item["id"].'" selected>'.$item["name"].'</option>');
                                            else
                                            echo ('<option value="'.$item["id"].'">'.$item["name"].'</option>');
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control select2bs4" id="role" name="role">
                                <option disabled selected>Hak Akses</option>
                                <?php
                                    if (isset($data)) {
                                        foreach ($data["roles"] as $item) {
                                            if ($item["id"] == $user["role_id"])
                                            echo ('<option value="'.$item["id"].'" selected>'.$item["name"].'</option>');
                                            else
                                            echo ('<option value="'.$item["id"].'">'.$item["name"].'</option>');
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control compose-textarea" placeholder="Tentang Saya" id="bio" name="bio"><?= $user["bio"] ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<!-- /.content -->