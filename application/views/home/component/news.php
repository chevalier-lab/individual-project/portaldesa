<div class="container mt-5">

    <div class="row">
        <div class="col-sm-8">
            <div class="row">
                <h4 class="mb-2"><?= $news['title'] ?></h4>
                <?php
                    $cover = $news["cover"];
                    echo ('
                    <div class="container"
                    style="display: block; 
                            height: 300px; 
                            background-size: cover; 
                            background-image: url('."'$cover'".')"></div>
                    ');
                ?>
                <div class="card card-widget container">
                    <div class="card-header row">
                        <div class="user-block">
                            <img class="img-circle" src="<?= $news['face'] ?>" alt="User Image">
                            <span class="username"><a href="#"><?= $news["name"] ?></a></span>
                            <span class="description"><?= $news["share_type"].' - '.$news["share_time"] ?></span>
                        </div>
                    </div>
                    <div class="card-body">
                        <p><?= $news["description"] ?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4"></div>
    </div>

</div>