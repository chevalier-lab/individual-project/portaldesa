<!-- SUPPORT BY -->
<div class="container">
    <h2 style="text-align: center"><?= $title; ?></h2>
    <table border="0" cellpadding="5" style="margin: 0 auto; border: none">
        <tbody>
            <tr>
                <?php
                    foreach ($items as $item) {
                        echo ('
                            <td style="margin: 0 auto; border: none"><img src="'.$item.'" style="max-height: 64px"></td>
                        ');
                    }
                ?>
            </tr>
        </tbody>
    </table>
</div>
<!-- SUPPORT BY -->