<!-- QUICK NEWS -->
<?php
    $this->load->view('home/helper/quickNews');
?>
<div class="container">
    <h2><?= $title; ?></h2>
    <div class="row">
        <?php
            foreach ($items as $item) {
                renderNews($item);
            }
        ?>
    </div>
</div>
<!-- QUICK NEWS -->