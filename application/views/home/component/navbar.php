<!-- Navbar -->
<?php 
    $this->load->view ("home/helper/navbar.php");
?>
<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
        <a href="#" class="navbar-brand">
            <img src=<?= $logo; ?> alt="AdminLTE Logo" class="brand-image"
                style="opacity: .8">
            <span class="brand-text font-weight-light"><?= $title; ?></span>
        </a>
    
        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <?php
                    if (count($items["left"]) > 0) {
                        foreach ($items["left"] as $item) {
                            if (count($item["child"]) == 0) {
                                renderAndcheckHaveIconOrNot($item);
                            }
                            else {
                                renderWithChild($item);
                            }
                        }
                    }
                ?>
            </ul>

            <!-- SEARCH FORM -->
            <?php
                if ($items["search"]) {
                    renderSearch();
                }
            ?>
        </div>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <?php
                if (count($items["right"]) > 0) {
                    foreach ($items["right"] as $item) {
                        if (count($item["child"]) == 0) {
                            renderAndcheckHaveIconOrNot($item);
                        }
                        else {
                            renderWithChild($item);
                        }
                    }
                }
            ?>
        </ul>
    </div>
</nav>
<!-- /.navbar -->