<!-- HEADING -->
<div class="container" style="margin-top: 16px">
    <div class="card card-widget">
        <div class="widget-user-header <?= $bgColor; ?>"
            style="background: url('<?= $bgImage; ?>') center center;">
            <div
                style="position: absolute; top: 0; left: 0; bottom: 0; right: 0;
                    background-color: rgba(0,0,0,0.5); z-index: 1"></div>
            <center
                style="position: relative; z-index: 2; margin-bottom: 2.5%">
                <img 
                    style="max-width: 10%; margin: 2.5%"
                    src="<?= $logo; ?>" 
                    alt="User Avatar">
                <h2 class="widget-user-username"><?= $caption; ?></h2>
                <h5 class="widget-user-desc"><?= $tagline; ?></h5>
            </center>
        </div>
    </div>
</div>
<!-- HEADING -->