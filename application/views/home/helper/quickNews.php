<?php
    function get_words($sentence, $count = 10) {
        preg_match("/(?:\w+(?:\W+|$)){0,$count}/", $sentence, $matches);
        return $matches[0];
    }
    
    function renderNews($item) {
        $description = explode(" ", strip_tags($item["description"]));
        $cover = $item["cover"];
        $location = $item["location"];
        if (count($description) > 25) {
            $description = implode(' ', array_slice($description, 0, 25)) . "...";
        }
        else {
            $description = implode(' ', $description);
        }
        echo ('
            <div class="col-md-4">
                <div class="card card-widget">
                    <div class="card-header">
                        <div class="user-block">
                            <img class="img-circle" src="'.$item["face"].'" alt="User Image">
                            <span class="username"><a href="#">'.$item["name"].'</a></span>
                            <span class="description">'.$item["share_type"].' - '.$item["share_time"].'</span>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div 
                            style="display: block; 
                                    height: 120px; 
                                    background-size: cover; 
                                    background-image: url('."'$cover'".')"></div>
                        <h4 style="cursor: pointer" onclick="location.assign('."'$location'".')">'.$item["title"].'</h4>
                        <p>'.$description.'</p>
                        <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i> Share</button>
                        <button type="button" class="btn btn-default btn-sm"><i class="far fa-thumbs-up"></i> Like</button>
                        <span class="float-right text-muted">127 likes - 3 comments</span>
                    </div>
                </div>
            </div>
        ');
    }
?>