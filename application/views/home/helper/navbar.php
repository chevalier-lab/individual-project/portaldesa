<?php
    function renderAndcheckHaveIconOrNot($item) {
        if (empty($item["icon"])) {
            echo ('
                <li class="nav-item">
                    <a 
                        href="'.$item["link"].'" 
                        class="nav-link '.$item["class"].'">
                        '.$item["text"].'
                    </a>
                </li>
            ');
        }
        else {
            echo ('
                <li class="nav-item">
                    <a 
                        href="'.$item["link"].'" 
                        class="nav-link '.$item["class"].'">
                        <i class="fas '.$item["icon"].'"></i>
                        '.$item["text"].'
                    </a>
                </li>
            ');
        }
    }

    function renderWithChild($item) {
        echo ('
            <li class="nav-item dropdown">
                <a 
                    id="'.$item["id"].'"
                    href="'.$item["link"].'" 
                    data-toggle="dropdown" 
                    aria-haspopup="true" 
                    aria-expanded="false" 
                    class="nav-link dropdown-toggle '.$item["class"].'">
                    '.$item["text"].'
                </a>
                <ul 
                    aria-labelledby="'.$item["id"].'" 
                    class="dropdown-menu border-0 shadow">
        ');
        foreach ($item["child"] as $child) {
            renderAndcheckHaveIconOrNot($child);
        }
        echo ('
                </ul>
            </li>');
    }

    function renderSearch() {
        echo ('
            <form class="form-inline ml-0 ml-md-3">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                    </button>
                    </div>
                </div>
            </form>
        ');
    }
?>