<?php
    $this->load->view('template/head.php', array(
        'title' => 'Portal Desa',
        'head_unit' => '
            <link rel="stylesheet" href='.base_url("assets/plugins/fontawesome-free/css/all.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/jqvmap/jqvmap.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/dist/css/adminlte.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/daterangepicker/daterangepicker.css").'>
            <link rel="stylesheet" href='.base_url("assets/plugins/summernote/summernote-bs4.css").'>
            <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        '
    ));
?>

<body class="hold-transition layout-top-nav">
    <div class="wrapper">
        <?php
            // Component Navbar
            $this->load->view('home/component/navbar', array(
                "logo"      => base_url("assets/dist/img/logo.png"),
                "title"     => "Desa Netpala",
                "items"     => array(
                    "left"      => array(),
                    "search"    => true,
                    "right"     => $menus
                )
            ));

            // Component Heading
            $this->load->view('home/component/heading', array(
                "bgColor"   => "bg-info",
                "bgImage"   => base_url("assets/dist/img/heading.jpg"),
                "logo"      => base_url("assets/dist/img/logo.png"),
                "caption"   => "Desa Netpala",
                "tagline"   => "Mollo Utara, Kabupaten Timor Tengah Selatan, Nusa Tenggara Timur."
            ));

            // Component Quick Recent News
            $this->load->view('home/component/quickNews', array(
                "title"     => "Berita Terbaru",
                "items"     => $lastNews
            ));

            // Component Copyright
            $this->load->view('/home/component/copyright');
        ?>
    </div>
</body>

<?php
    $this->load->view('template/foot.php', array(
        'foot_unit' => '
            <script src='.base_url("assets/plugins/jquery/jquery.min.js").'></script>
            <script src='.base_url("assets/plugins/jquery-ui/jquery-ui.min.js").'></script>
            <script src='.base_url("assets/plugins/bootstrap/js/bootstrap.bundle.min.js").'></script>
            <script src='.base_url("assets/plugins/chart.js/Chart.min.js").'></script>
            <script src='.base_url("assets/plugins/sparklines/sparkline.js").'></script>
            <script src='.base_url("assets/plugins/jqvmap/jquery.vmap.min.js").'></script>
            <script src='.base_url("assets/plugins/jqvmap/maps/jquery.vmap.usa.js").'></script>
            <script src='.base_url("assets/plugins/jquery-knob/jquery.knob.min.js").'></script>
            <script src='.base_url("assets/plugins/moment/moment.min.js").'></script>
            <script src='.base_url("assets/plugins/daterangepicker/daterangepicker.js").'></script>
            <script src='.base_url("assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js").'></script>
            <script src='.base_url("assets/plugins/summernote/summernote-bs4.min.js").'></script>
            <script src='.base_url("assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js").'></script>
            <script src='.base_url("assets/dist/js/adminlte.js").'></script>
            <script src='.base_url("assets/dist/js/pages/dashboard.js").'></script>
            <script src='.base_url("assets/dist/js/demo.js").'></script>
            <script>
                $.widget.bridge("uibutton", $.ui.button)
            </script>
        '
    ));
?>