<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerModel extends CI_Controller {
    public function __construct() {
        parent::__construct();
        // Load All Model
        $this->load->model('menus/MenuModel');
        $this->load->model('users/UserModel');
        $this->load->model('pages/PageModel');
        $this->load->model('posts/PostModel');
        $this->load->model('tags/TagModel');
        $this->load->model('tags/TagPostModel');
        $this->load->model('tags/TagPageModel');
        $this->load->model('medias/MediaModel');
        $this->load->model('departments/DepartmentModel');
        $this->load->model('roles/RoleModel');
        $this->load->model('comments/CommentModel');
    }

    // Menu Models
    public function getMenuModel() {
        return $this->MenuModel;
    }

    // User Models
    public function getUserModel() {
        return $this->UserModel;
    }

    // Page Models
    public function getPageModel() {
        return $this->PageModel;
    }

    // Post Models
    public function getPostModel() {
        return $this->PostModel;
    }

    // Tag Models
    public function getTagModel() {
        return $this->TagModel;
    }

    // Tag Post Models
    public function getTagPostModel() {
        return $this->TagPostModel;
    }

    // Tag Page Models
    public function getTagPageModel() {
        return $this->TagPageModel;
    }

    // Media Models
    public function getMediaModel() {
        return $this->MediaModel;
    }

    // Department Models
    public function getDepartmentModel() {
        return $this->DepartmentModel;
    }

    // Role Models
    public function getRoleModel() {
        return $this->RoleModel;
    }

    //Comment Models
    public function getCommentModel(){
        return $this->CommentModel;
    }
}