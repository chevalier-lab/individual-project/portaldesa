<?php

class HomeViews extends CI_Controller {
    private $models;
    public function __construct($models) {
        parent::__construct();
        $this->models = $models;
    }

    // Get Active Menus
    private function menus() {
        $selection =  array(
            'm_menus.id menu_id',
            'm_menus.label',
            'm_menus.link',
            'm_menus.icon',
            'm_menus.class',
            'm_menus.parent_id',
            'm.label parent_label'
        );

        // Join Description
        $join = array(
            'table' => array(
                'column'    => 'm_menus m',
                'on'        => 'm_menus.parent_id = m.id'
            )
        );

        // Process Select All Menus With Join to Parent Menu
        $data = $this->models->getMenuModel()->getMenusWithJoin($selection, $join);

        $response = array();

        // Get Root Menu
        foreach ($data as $item) {
            if (empty($item["parent_id"]))
                $response[] = array(
                    "id"    => "menuItem_" . $item["menu_id"],
                    "link"  => $item["link"],
                    "text"  => $item["label"],
                    "class" => $item["class"],
                    "icon"  => $item["icon"],
                    "child" => array()
                );
        }

        $response[] = array(
            "id"    => "",
            "link"  => base_url("index.php/controller/login"),
            "text"  => "Masuk",
            "class" => "btn btn-primary text-white",
            "icon"  => "fa-sign-in-alt",
            "child" => array()
        );

        // Get Child Menu
        foreach ($data as $item) {
            if (!empty($item["parent_id"])) {
                $position = 0;
                foreach ($response as $root) {
                    if ($root["id"] == "menuItem_" . $item["parent_id"]) {
                        $response[$position]["child"][] = array(
                            "id"    => "menuItem_" . $item["menu_id"],
                            "link"  => $item["link"],
                            "text"  => $item["label"],
                            "class" => $item["class"],
                            "icon"  => $item["icon"]
                        );
                    }
                    $position++;
                }
            }
        }

        return $response;
    }

    // Get Last Three News
    private function getLastThreeNews() {
        $data = $this->models->getPostModel()->getJoinWithSelection(
            array(
                "m_posts.*",
                "m.name cover_name",
                "m.uri cover_location",
                "u.name full_name"
            ),
            array(
                array(
                    "table" => "m_users u",
                    "on"    => "u.id=m_posts.user_id"
                ),
                array(
                    "table" => "m_medias m",
                    "on"    => "m.id=m_posts.cover_id"
                )
            )
        );

        $response = array();
        foreach ($data as $item) {
            $response[] = array(
                "face"  => base_url("assets/dist/img/logo.png"),
                "name"  => $item["full_name"],
                "share_type"    => ($item["status"] == 1) ? "Shared publicly":"Shared privately",
                "share_time"    => "7:30 PM Today",
                "cover"         => $item["cover_location"] . $item["cover_name"],
                "description"   => $item["content"],
                "title"         => $item["title"],
                "location"      => base_url("index.php/controller/news?uri=") . $item["uri"]
            );
        }

        return $response;
    }

    private function getOnceNews($uri) {
        $data = $this->models->getPostModel()->getJoinWithSelectionAndWhere(
            array(
                "m_posts.*",
                "m.name cover_name",
                "m.uri cover_location",
                "u.name full_name"
            ),
            array(
                array(
                    "table" => "m_users u",
                    "on"    => "u.id=m_posts.user_id"
                ),
                array(
                    "table" => "m_medias m",
                    "on"    => "m.id=m_posts.cover_id"
                )
            ),
            array(
                "m_posts.uri"   => $uri
            )
        );

        $response = array();
        foreach ($data as $item) {
            $response[] = array(
                "face"  => base_url("assets/dist/img/logo.png"),
                "name"  => $item["full_name"],
                "share_type"    => ($item["status"] == 1) ? "Shared publicly":"Shared privately",
                "share_time"    => "7:30 PM Today",
                "cover"         => $item["cover_location"] . $item["cover_name"],
                "description"   => $item["content"],
                "title"         => $item["title"],
                "location"      => base_url("index.php/controller/news?uri=") . $item["uri"]
            );
        }

        return $response;
    }

    private function getOncePages($uri) {
        $data = $this->models->getPageModel()->getJoinWithSelectionAndWhere(
            array(
                "m_pages.*",
                "m.name cover_name",
                "m.uri cover_location",
                "u.name full_name"
            ),
            array(
                array(
                    "table" => "m_users u",
                    "on"    => "u.id=m_pages.user_id",
                    "direction" => ""
                ),
                array(
                    "table" => "m_medias m",
                    "on"    => "m.id=m_pages.cover_id",
                    "direction" => "left"
                )
            ),
            array(
                "m_pages.uri"   => $uri
            )
        );

        $response = array();
        foreach ($data as $item) {
            $response[] = array(
                "face"  => base_url("assets/dist/img/logo.png"),
                "name"  => $item["full_name"],
                "share_type"    => ($item["status"] == 1) ? "Shared publicly":"Shared privately",
                "share_time"    => "7:30 PM Today",
                "cover"         => $item["cover_location"] . $item["cover_name"],
                "description"   => $item["content"],
                "title"         => $item["title"],
                "location"      => base_url("index.php/controller/pages?uri=") . $item["uri"]
            );
        }

        return $response;
    }

    // Display Landing Page
	public function index() {
        $this->load->view('home/landing', array(
            "menus" => $this->menus(),
            "lastNews"  => $this->getLastThreeNews()
        ));
    }

    // Display News Page
    public function news() {

        $uri = $this->input->get("uri") ?: "";
        if (empty($uri)) {
            // Do Not Found
            die();
        }

        $this->load->view('home/news', array(
            "menus" => $this->menus(),
            "news"  => $this->getOnceNews($uri)
        ));

    }

    // Display Pages Page
    public function pages() {

        $uri = $this->input->get("uri") ?: "";
        if (empty($uri)) {
            // Do Not Found
            die();
        }

        $this->load->view('home/pages', array(
            "menus" => $this->menus(),
            "pages"  => $this->getOncePages($uri)
        ));

    }
}
