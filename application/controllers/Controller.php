<?php
defined('BASEPATH') or exit('No direct script access allowed');

// Core
require_once('ControllerModel.php');
require_once('ControllerAuth.php');

// Views Only
require_once('home/HomeViews.php');
require_once('dashboard/DashboardViews.php');
require_once('portal/PortalViews.php');

// Actions Only
require_once('dashboard/DashboardActions.php');
require_once('portal/PortalActions.php');

// Seeder Only
require_once('seeder/InitialSeeder.php');

class Controller extends CI_Controller
{
    private $routes, $models, $auth, $seed;
    public function __construct()
    {
        $this->__init();
        parent::__construct();
        $this->__library();
    }

    public function __init()
    {
        // Core
        $this->routes = array();
        $this->models = new ControllerModel();
        $this->auth = new ControllerAuth($this->models);

        // Views Only
        $this->routes['home.views'] = new HomeViews($this->models);
        $this->routes['dashboard.views'] = new DashboardViews($this->models);
        $this->routes['portal.views'] = new PortalViews($this->models);

        // Seeder Only
        $this->seed = new InitialSeeder($this->models);
    }

    public function __library()
    {
        // Actions Only
        $this->routes['portal.actions'] = new PortalActions($this->models, $this->auth);
        $this->routes['dashboard.actions'] = new DashboardActions($this->models);
    }

    // Initial Seeder Tree
    public function installation()
    {
        $this->seed->index();
    }

    // Initial Helper Tree
    public function checkAuth()
    {
        return $this->auth->getSession();
    }

    // Dashboard Tree
    public function dashboard()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->index();
    }

    public function createNews()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->createNews();
    }

    public function updateNews()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->news_update();
    }

    public function listNews()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->listNews();
    }

    public function createPages()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->createPages();
    }

    public function updatePages()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->pages_update();
    }

    public function listPages()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->listPages();
    }

    public function menus()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->menus();
    }

    public function createMenus()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->menus_create();
    }

    public function updateMenus()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->menus_update();
    }

    public function users()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->users();
    }

    public function usersCreate()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->users_create();
    }

    public function usersUpdate()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->users_update();
    }

    public function list_departments()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->list_departments();
    }

    public function departmentsCreate()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->departments_create();
    }

    public function departmentsUpdate()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->departments_update();
    }

    public function list_tags()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->list_tags();
    }

    public function create_tags()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->create_tags();
    }
    public function update_tags()
    {
        if ($this->checkAuth() == null) redirect('/controller/login', 'refresh');
        else return $this->routes['dashboard.views']->update_tags();
    }

    // ---------------------------------------------------------- Actions in Dashboard for CRUD user
    public function users_action_create()
    {
        $this->routes['dashboard.actions']->doCreateUser();
    }

    public function users_action_read_all()
    {
        $this->routes['dashboard.actions']->doReadAll();
    }

    public function users_action_read_once()
    {
        $this->routes['dashboard.actions']->doReadOnce();
    }

    public function users_action_delete()
    {
        $this->routes['dashboard.actions']->doDeleteUser();
    }

    public function users_action_update()
    {
        $this->routes['dashboard.actions']->doUpdateUser();
    }
    // ---------------------------------------------------------- End of CRUD user

    // ---------------------------------------------------------- Actions in Dashboard for CR News
    public function news_action_create()
    {
        $this->routes['dashboard.actions']->doCreateNews();
    }

    public function news_action_delete()
    {
        $this->routes['dashboard.actions']->doDeleteNews();
    }

    public function news_action_update()
    {
        $this->routes['dashboard.actions']->doUpdateNews();
    }

    public function news_action_read_all()
    {
        $this->routes['dashboard.actions']->doReadAllNews();
    }

    public function news_action_read_once()
    {
        $this->routes['dashboard.actions']->doReadOnceNews();
    }
    // ---------------------------------------------------------- End of CR News

    // ---------------------------------------------------------- Actions in Dashboard for CRUD Menu
    public function menus_action_create()
    {
        $this->routes['dashboard.actions']->doCreateMenu();
    }

    public function menus_action_read_all()
    {
        $this->routes['dashboard.actions']->doReadAllMenu();
    }

    public function menus_action_read_once()
    {
        $this->routes['dashboard.actions']->doReadOnceMenu();
    }

    public function menus_action_delete()
    {
        $this->routes['dashboard.actions']->doDeleteMenu();
    }

    public function menus_action_update()
    {
        $this->routes['dashboard.actions']->doUpdateMenu();
    }
    // ---------------------------------------------------------- End of CRUD Menu

    // ---------------------------------------------------------- Actions in Dashboard for CR Page
    public function pages_action_create()
    {
        $this->routes['dashboard.actions']->doCreatePage();
    }

    public function pages_action_update()
    {
        $this->routes['dashboard.actions']->doUpdatePages();
    }

    public function pages_action_delete()
    {
        $this->routes['dashboard.actions']->doDeletePage();
    }

    public function pages_action_read()
    {
        $this->routes['dashboard.actions']->doReadPage();
    }

    public function pages_action_read_all()
    {
        $this->routes['dashboard.actions']->doReadAllPages();
    }
    // ---------------------------------------------------------- End of CR Pages

    // ---------------------------------------------------------- Actions in Dashboard for CRUD Tag
    public function tag_action_create()
    {
        $this->routes['dashboard.actions']->doCreateTag();
    }

    public function tag_action_read_all()
    {
        $this->routes['dashboard.actions']->doReadAllTag();
    }

    public function tag_action_read_once()
    {
        $this->routes['dashboard.actions']->doReadOnceTag();
    }

    public function tag_action_delete()
    {
        $this->routes['dashboard.actions']->doDeleteTag();
    }

    public function tag_action_update()
    {
        $this->routes['dashboard.actions']->doUpdateTag();
    }
    // ---------------------------------------------------------- End of CRUD tag

    // ---------------------------------------------------------- Counter On Dashboard
    public function counter_dashboard()
    {
        $this->routes['dashboard.actions']->doCounterDashboard();
    }
    // ---------------------------------------------------------- End of Counter On Dashboard

    // ---------------------------------------------------------- Actions in Dashboard for CRUD Department
    public function department_action_create()
    {
        $this->routes['dashboard.actions']->doCreateDepartment();
    }

    public function department_action_read_all()
    {
        $this->routes['dashboard.actions']->doReadAllDepartment();
    }

    public function department_action_read_once()
    {
        $this->routes['dashboard.actions']->doReadOnceDepartment();
    }

    public function department_action_delete()
    {
        $this->routes['dashboard.actions']->doDeleteDepartment();
    }

    public function department_action_update()
    {
        $this->routes['dashboard.actions']->doUpdateDepartment();
    }
    // ---------------------------------------------------------- End of CRUD Department

    // Portal Tree
    public function login()
    {
        return $this->routes['portal.views']->index();
    }

    public function registration()
    {
        return $this->routes['portal.views']->registration();
    }

    // ---------------------------------------------------------- Actions in Portal Do Sign In
    public function portal_action_sign_in()
    {
        $this->routes['portal.actions']->doSignIn();
    }

    public function portal_action_sign_out()
    {
        $res = $this->routes['portal.actions']->doSignOut();
        if ($res != null)
            redirect($res);
        else
            redirect(base_url('index.php/controller/dashboard'));
    }

    /*------------------------------------------------------------------------ Display Home */
    
    // Home Tree
    public function index()
    {
        return $this->routes['home.views']->index();
    }

    // News
    public function news()
    {
        return $this->routes['home.views']->news();
    }

    public function pages()
    {
        return $this->routes['home.views']->pages();
    }

    /*------------------------------------------------------------------------ Home Action */
    // Get Three Last News
    public function three_last_news()
    {
        $this->routes['dashboard.actions']->doReadThreeLastNews();
    }
}
