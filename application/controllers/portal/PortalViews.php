<?php

class PortalViews extends CI_Controller {
    private $models;
    public function __construct($models) {
        parent::__construct();
        $this->models = $models;
    }

    // Display Login
	public function index() {
        $this->load->view('portal/index', array(
            "location"  => "portal/login",
            "page"      => "Portal Masuk"
        ));
    }

    // Display Registration
    public function registration() {
        $this->load->view('portal/index', array(
            "location"  => "portal/registration",
            "page"      => "Portal Daftar"
        ));
    }
}
