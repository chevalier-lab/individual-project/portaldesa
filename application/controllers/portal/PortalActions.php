<?php

class PortalActions extends CI_Controller
{
    private $models, $auth;
    public function __construct($models, $auth)
    {
        $this->models = $models;
        $this->auth = $auth;
        parent::__construct();
    }

    // -------------------------------------------------------------------------------------- SIGN IN ACTION START
    // Sign In User
    public function doSignIn() {
        // Do Authentication
        $auth = $this->auth->auth();
        // Set Default Response
        $res = array(
            'data'  => array(
                'status'    => 'error',
                'code'      => 500,
                'message'   => 'Username / email atau password tidak ditemukan'
            )
        );
        // Check If Authentication Success
        if ($auth != null) {
            // Create Session
            if ($this->auth->createSession($auth)) {
                // Change Default Response to Success Response
                $res['data'] = array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Berhasil melakukan authentikasi',
                    'hotlink'   => base_url('index.php/controller/dashboard')
                );
            }
        }
        // Return Response
        echo json_encode($res, JSON_PRETTY_PRINT);
    }
    // -------------------------------------------------------------------------------------- SIGN IN ACTION END
    // -------------------------------------------------------------------------------------- SIGN OUT ACTION START
    // Sign Out User
    public function doSignOut() {
        // Do Authentication
        $auth = $this->auth->getSession();
        // Set Default Response
        $res = null;
        // Check If Authentication Success
        if ($auth != null) {
            // Destroy Session
            if ($this->auth->destroySession()) {
                // Change Default Response to Success Response
                $res = base_url('index.php/controller/login');
            }
        }
        // Return Response
        return $res;
    }
    // -------------------------------------------------------------------------------------- SIGN OUT ACTION END
}
