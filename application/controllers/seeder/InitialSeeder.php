<?php

class InitialSeeder extends CI_Controller {
    private $models;
    public function __construct($models) {
        parent::__construct();
        $this->models = $models;
    }

    // Running Seeder
	public function index() {
        $this->createRoles();
        $this->createDepartments();
        $this->createFaces();
        $this->createAdministrator();
    }

    // Create Data Roles
    private function createRoles() {
        $dataSeed = array(
            'Administrator',
            'Direktur',
            'Pegawai'
        );
        echo ('Membuat seed role <br>');
        foreach ($dataSeed as $item) {
            $this->models->getRoleModel()->create(array(
                'name'  => $item
            ));
        }
        echo ('Seed role berhasil dibuat <br>');
    }

    // Create Data Departments
    private function createDepartments() {
        $dataSeed = array(
            'Administrator',
            'Kepala Desa',
            'BPD',
            'Sekretaris',
            'KAUR',
            'Kepala Dusun'
        );
        echo ('Membuat seed department <br>');
        foreach ($dataSeed as $item) {
            $this->models->getDepartmentModel()->create(array(
                'name'  => $item
            ));
        }
        echo ('Seed department berhasil dibuat <br>');
    }

    // Create Data Face
    private function createFaces() {
        $dataSeed = array(
            'avatar.png'
        );
        echo ('Membuat seed face <br>');
        foreach ($dataSeed as $item) {
            $this->models->getMediaModel()->create(array(
                'name'  => $item,
                'uri'   => base_url('assets/dist/img/')
            ));
        }
        echo ('Seed face berhasil dibuat <br>');
    }

    // Create Data Administrator
    private function createAdministrator() {
        $dataSeed = array(
            'name'      => 'Administrator',
            'email'     => 'andymul@student.telkomuniversity.ac.id',
            'password'  => md5('programer'),
            'bio'       => 'Im Iron Man',
            'face_id'   => 1,
            'department_id' => 1,
            'role_id'   => 1,
            'api_key'   => md5('programer')
        );
        echo ('Membuat seed administrator <br>');
        $this->models->getUserModel()->create($dataSeed);
        echo ('Seed administrator berhasil dibuat <br>');
    }
}