<?php

class DashboardViews extends CI_Controller {
    private $models;
    public function __construct($models) {
        parent::__construct();
        $this->models = $models;
    }

    // Display Index
	public function index() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/dashboard",
            "page"      => "Dashboard",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Dashboard",
                    "active"    => true
                )
            )
        ));
    }
    // Display Create News
    public function createNews() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/create_news",
            "page"      => "Buat Berita",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Berita",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Buat Berita",
                    "active"    => true
                )
            )
        ));
    }// Display News Update
    public function news_update() {
        $news_id = $this->input->get("news_id");

        $selection =  array(
            'select' => array(
                'm_posts.id',
                'm_posts.title',
                'm_posts.content',
                'm_posts.uri',
                'm_posts.status',
                'm_posts.user_id',
                'm.name cover',
                'm.uri cover_uri',
                'm.id cover_id'
            )
        );
        $join = array(
            'table1' => array(
                'column'    => 'm_medias m',
                'on'        => 'm_posts.cover_id = m.id'
            )
        );

        // Find Specific id user From `m_posts` table
        $where = array(
            'm_posts.id' => $news_id
        );
        
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/update_news",
            "page"      => "Edit Berita",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Berita",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Edit Berita",
                    "active"    => true
                )
            ),
            "data"      => array(
                "news"  => $this->models->getPostModel()->getPostWithStatus($selection, $join, $where)[0],
                "tags"  => $this->models->getTagPostModel()->getTagPostWithWhereJoin(
                    array(
                        'select'    => array(
                            't_tags_posts.id',
                            'mt.label'
                        )
                    ), array(
                        'table1' => array(
                            'column'    => 'm_tags mt',
                            'on'        => 't_tags_posts.tag_id = mt.id'
                        ),
                        'table2' => array(
                            'column'    => 'm_posts mp',
                            'on'        => 't_tags_posts.post_id = mp.id'
                        )
                    ), array(
                        'mp.id' => $news_id
                    )
                )
            )
        ));
    }
    // Display List News
    public function listNews() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/list_news",
            "page"      => "Daftar Berita",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Berita",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Daftar Berita",
                    "active"    => true
                )
            )
        ));
    }
    // Display Create Pages
    public function createPages() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/create_pages",
            "page"      => "Buat Halaman",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Halaman",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Buat Halaman",
                    "active"    => true
                )
            )
        ));
    }
    // Display Pages Update
    public function pages_update() {
        $page_id = $this->input->get("page_id");

        $selection =  array(
            'select' => array(
                'm_pages.id',
                'm_pages.title',
                'm_pages.content',
                'm_pages.uri',
                'm_pages.status',
                'm_pages.user_id',
                'm.name cover',
                'm.uri cover_uri',
                'm.id cover_id'
            )
        );
        $join = array(
            'table1' => array(
                'column'    => 'm_medias m',
                'on'        => 'm_pages.cover_id = m.id',
                'direction' => 'left'
            )
        );

        // Find Specific id user From `m_pages` table
        $where = array(
            'm_pages.id' => $page_id
        );
        
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/update_pages",
            "page"      => "Edit Halaman",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Halaman",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Edit Halaman",
                    "active"    => true
                )
            ),
            "data"      => array(
                "pages"  => $this->models->getPageModel()->getPageWithStatus($selection, $join, $where)[0],
                "tags"  => $this->models->getTagPageModel()->getTagPageWithWhereJoin(
                    array(
                        'select'    => array(
                            't_tags_pages.id',
                            'mt.label'
                        )
                    ), array(
                        'table1' => array(
                            'column'    => 'm_tags mt',
                            'on'        => 't_tags_pages.tag_id = mt.id'
                        ),
                        'table2' => array(
                            'column'    => 'm_pages mp',
                            'on'        => 't_tags_pages.page_id = mp.id'
                        )
                    ), array(
                        'mp.id' => $page_id
                    )
                )
            )
        ));
    }
    // Display List Pages
    public function listPages() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/list_pages",
            "page"      => "Daftar Halaman",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Halaman",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Daftar Halaman",
                    "active"    => true
                )
            )
        ));
    }
    // Display Menus
    public function menus() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/menus",
            "page"      => "Menu",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Menu",
                    "active"    => true
                )
            )
        ));
    }
    // Display Menus Create
    public function menus_create() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/create_menus",
            "page"      => "Buat Menu",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Menu",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Buat Menu",
                    "active"    => true
                )
            ),
            "data"      => array(
                "parent"          => $this->models->getMenuModel()->all()
            )
        ));
    }
    // Display Menus Update
    public function menus_update() {
        $menu_id = $this->input->get("menu_id");

        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/update_menus",
            "page"      => "Edit Menu",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Menu",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Edit Menu",
                    "active"    => true
                )
            ),
            "data"      => array(
                "parent"          => $this->models->getMenuModel()->all(),
                "menu"            => $this->models->getMenuModel()->getBy(
                    array("id"  => $menu_id)
                )[0]
            )
        ));
    }
    // Display Users
    public function users() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/users",
            "page"      => "Pengguna",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Pengguna",
                    "active"    => true
                )
            )
        ));
    }
    // Display Users Create
    public function users_create() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/create_users",
            "page"      => "Buat Pengguna",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Pengguna",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Buat Pengguna",
                    "active"    => true
                )
            ),
            "data"      => array(
                "departments"    => $this->models->getDepartmentModel()->all(),
                "roles"          => $this->models->getRoleModel()->all()
            )
        ));
    }
    // Display Users Update
    public function users_update() {
        $user_id = $this->input->get('user_id');

        $selection =  array(
            'select' => array(
                'm_users.id user_id',
                'm_users.name',
                'm_users.email',
                'm_users.bio',
                'm_users.status',
                'd.name department',
                'd.id department_id',
                'r.name role',
                'r.id role_id',
                'm.name face',
                'm.uri face_uri',
                'm.id face_id'
            )
        );
        $join = array(
            'table1' => array(
                'column'    => 'm_departments d',
                'on'        => 'm_users.department_id = d.id'
            ),
            'table2' => array(
                'column'    => 'm_roles r',
                'on'        => 'm_users.role_id = r.id'
            ),
            'table3' => array(
                'column'    => 'm_medias m',
                'on'        => 'm_users.face_id = m.id'
            )
        );

        // Find Specific id user From `m_users` table
        $where = array(
            'm_users.id' => $user_id
        );

        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/update_users",
            "page"      => "Edit Pengguna",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Pengguna",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Edit Pengguna",
                    "active"    => true
                )
            ),
            "data"      => array(
                "departments"    => $this->models->getDepartmentModel()->all(),
                "roles"          => $this->models->getRoleModel()->all(),
                "user"           => $this->models->getUserModel()->getUserWithStatus($selection, $join, $where)[0]
            )
        ));
    }
    // Display Departments
    public function list_departments() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/list_departments",
            "page"      => "Daftar Jabatan",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Master Data",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Daftar Jabatan",
                    "active"    => true
                )
            )
        ));
    }
    // Display Departments Create
    public function departments_create() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/create_departments",
            "page"      => "Buat Jabatan",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Master Data",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Jabatan",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Buat Jabatan",
                    "active"    => true
                )
            )
        ));
    }
    // Display Departments Update
    public function departments_update() {
        $department_id = $this->input->get("department_id");
        
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/update_departments",
            "page"      => "Edit Jabatan",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Master Data",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Jabatan",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Edit Jabatan",
                    "active"    => true
                )
            ),
            "data"      => array(
                "department"            => $this->models->getDepartmentModel()->getBy(
                    array("id"  => $department_id)
                )[0]
            )
        ));
    }
    // Display Tags Create
    public function create_tags()
    {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/create_tags",
            "page"      => "Buat Tag",
            "page"      => "Buat Tags",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Tag",
                    "text"      => "Master Data",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Buat Tags",
                    "active"    => true
                )
            )
        ));
    }
    // Display Tags Update
    public function update_tags()
    {
        $tag_id = $this->input->get("tag_id");

        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/update_tags",
            "page"      => "Edit Menu",
            "page"      => "Edit Tags",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Tag",
                    "text"      => "Master Data",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Edit Tag",
                    "text"      => "Tags",
                    "active"    => true
                )
            ),
            "data"      => array(
                "tag"            => $this->models->getTagModel()->getBy(
                    array("id"  => $tag_id)
                )[0]
            )
        ));
    }
    // Display Tags
    public function list_tags() {
        $this->load->view('dashboard/index', array(
            "location"  => "dashboard/list_tags",
            "page"      => "Daftar Tags",
            "indexing"  => array(
                array(
                    "link"      => "#",
                    "text"      => "Master Data",
                    "active"    => false
                ),
                array(
                    "link"      => "#",
                    "text"      => "Daftar Tags",
                    "active"    => true
                )
            )
        ));
    }
}
