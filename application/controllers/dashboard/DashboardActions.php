<?php

class DashboardActions extends CI_Controller
{
    private $models;
    private $fileUpload;
    public function __construct($models)
    {
        $this->models = $models;
        parent::__construct();
        // Load Validation
        $this->load->library("form_validation");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg"
                ),
                "max_size"  => 2000000
            )
        );
    }

    // -------------------------------------------------------------------------------------- USER ACTION START
    // Create User
    public function doCreateUser()
    {
        // Set Rule Validation
        $this->form_validation->set_rules('name', 'Name', 'required|max_length[100]');
        $this->form_validation->set_rules('email', 'E-Mail', 'required|max_length[100]|is_unique[m_users.email]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[255]');

        // Check Validation Rule
        if ($this->form_validation->run() == FALSE) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => validation_errors()
                )
            );
        } else {
            // Insert Photo First
            $photos = $this->fileUpload->do_upload("photos");
            if ($photos["status"]) {
                $this->models->getMediaModel()->create(array(
                    "name"  => $photos["file_name"],
                    "uri"   => base_url("assets/dist/img/")
                ));
                $face_id = $this->models->getMediaModel()->getID();

                // Prepare To Insert New User
                $new_user = array(
                    'name'              => $this->input->post('name'),
                    'email'             => $this->input->post('email'),
                    'password'          => md5($this->input->post('password')),
                    'bio'               => $this->input->post('bio') ?: '',
                    'status'            => $this->input->post('status') ?: 1,
                    'face_id'           => $face_id ?: 1,
                    'department_id'     => $this->input->post('department') ?: 1,
                    'role_id'           => $this->input->post('role') ?: 3,
                    'api_key'           => md5($this->input->post('password') . date('YmdHis')),
                );

                // Create New user
                if ($this->models->getUserModel()->create($new_user))
                    $res = array(
                        'data'  => array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => "Berhasil membuat data pengguna"
                        )
                    );
                else
                    $res = array(
                        'data'  => array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => "Gagal membuat data pengguna"
                        )
                    );
            } else
                $res = array(
                    'data'  => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => "Gagal membuat data pengguna"
                    )
                );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Read All User
    public function doReadAll()
    {
        $selection =  array(
            'select' => array(
                'm_users.id',
                'm_users.name',
                'm_users.email',
                'm_users.status',
                'd.name department',
                'r.name role'
            )
        );
        $join = array(
            'table1' => array(
                'column'    => 'm_departments d',
                'on'        => 'm_users.department_id = d.id'
            ),
            'table2' => array(
                'column'    => 'm_roles r',
                'on'        => 'm_users.role_id = r.id'
            )
        );

        // Process Select All User With Join to Department
        $data = $this->models->getUserModel()->getUsersWithStatus($selection, $join);

        // Check The Result of Process Above
        if ($data) {
            $res = array(
                'data' => array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Berhasil mengambil data pengguna',
                    'data'      => $data
                )
            );
        } else {
            $res = array(
                'data' => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'Gagal mengambil data pengguna',
                    'data'      => []
                )
            );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Read One User
    public function doReadOnce()
    {
        $selection =  array(
            'select' => array(
                'm_users.id user_id',
                'm_users.name',
                'm_users.email',
                'm_users.bio',
                'm_users.status',
                'd.name department',
                'r.name role',
                'm.uri face',
                'm.id face_id'
            )
        );
        $join = array(
            'table1' => array(
                'column'    => 'm_departments d',
                'on'        => 'm_users.department_id = d.id'
            ),
            'table2' => array(
                'column'    => 'm_roles r',
                'on'        => 'm_users.role_id = r.id'
            ),
            'table3' => array(
                'column'    => 'm_medias m',
                'on'        => 'm_users.face_id = m.id'
            )
        );

        // Find Specific id user From `m_users` table
        $where = array(
            'm_users.id' => $this->input->get('user_id')
        );

        // Cek if there is id user available
        if (!isset($_GET['user_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID user dibutuhkan'
                )
            );
        } else {
            // Process Select Specific User With Join to Department
            $data = $this->models->getUserModel()->getUserWithStatus($selection, $join, $where);
            if ($data) {
                $res = array(
                    'data' => array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Berhasil mengambil data pengguna',
                        'data'      => $data
                    )
                );
            } else {
                $res = array(
                    'data' => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'Gagal mengambil data pengguna',
                        'data'      => []
                    )
                );
            }
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Delete One User
    public function doDeleteUser()
    {

        // Find Specific id user From `m_users` table
        $where = array(
            'm_users.id' => $this->input->get('user_id')
        );

        // Cek if there is id user available
        if (!isset($_GET['user_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID user dibutuhkan'
                )
            );
        } else {
            // Delete User With Id
            if ($this->models->getUserModel()->delete($where)) {
                $res = array(
                    "data" => array(
                        "status"    => "success",
                        "code"      => 200,
                        "message"   => "Berhasil menghapus data pengguna"
                    )
                );
            } else {
                $res = array(
                    "data" => array(
                        "status"    => "error",
                        "code"      => 500,
                        "message"   => "Terjadi kesalahan akun tidak ditemukan"
                    )
                );
            }
        }
        // Return The Response
        echo json_encode($res);
    }

    // Update User
    public function doUpdateUser()
    {

        // Set Rule Validation

        // If user update they name, name must be below 100 char
        $this->form_validation->set_rules('name', 'Name', 'max_length[100]');

        // If user update they email, confirm that email not stored in database
        $this->form_validation->set_rules('email', 'E-Mail', 'max_length[100]');

        // If user update they password, password must be below 100 char
        $this->form_validation->set_rules('password', 'Password', 'max_length[255]');

        $where = array(
            'm_users.id' => $this->input->post('user_id')
        );

        // Cek if there is id user available
        if (!isset($_POST['user_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID user dibutuhkan'
                )
            );
        } else {
            // Check Validation Rule
            if ($this->form_validation->run() == FALSE) {
                $res = array(
                    'data'  => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => validation_errors()
                    )
                );
            } else {
                $photos = $this->fileUpload->do_upload("photos");
                $face_id = '';
                if ($photos["status"]) {
                    $this->models->getMediaModel()->create(array(
                        "name"  => $photos["file_name"],
                        "uri"   => base_url("assets/dist/img/")
                    ));
                    $face_id = $this->models->getMediaModel()->getID();
                }

                $userData = $this->models->getUserModel()->getBy($where)[0];

                // Prepare To Update User
                $user_update = array(
                    'name'              => $this->input->post('name') ?: $userData['name'],
                    'email'             => $this->input->post('email') ?: $userData['email'],
                    'password'          => $this->input->post('password') ? md5($this->input->post('password')) : $userData['password'],
                    'bio'               => $this->input->post('bio') ?: $userData['bio'],
                    'status'            => $this->input->post('status') ?: $userData['status'],
                    'face_id'           => $face_id != '' ? $face_id : $userData['face_id'],
                    'department_id'     => $this->input->post('department') ?: $userData['department_id'],
                    'role_id'           => $this->input->post('role') ?: $userData['role_id'],
                );
                // Update user
                if ($this->models->getUserModel()->update($where, $user_update))
                    $res = array(
                        'data'  => array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => "Berhasil memperbarui data pengguna",
                            'data'      => $user_update
                        )
                    );
                else
                    $res = array(
                        'data'  => array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => "Terjadi kesalahan pada email/password",
                            'data'      => $user_update
                        )
                    );
            }
        }

        // Retrun The Response
        echo json_encode($res);
    }
    // -------------------------------------------------------------------------------------- END OF USER ACTION

    // -------------------------------------------------------------------------------------- NEWS ACTION START
    //Create News
    public function doCreateNews()
    {
        //Set Rule Validation
        $this->form_validation->set_rules('title', 'Title', 'required|max_length[255]');
        $this->form_validation->set_rules('content', 'Content', 'required');

        if ($this->form_validation->run() == FALSE) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => validation_errors()
                )
            );
        } else {
            // Prepare To Insert New News
            // Insert Photo First
            $photos = $this->fileUpload->do_upload("photos");
            if ($photos["status"]) {
                $this->models->getMediaModel()->create(array(
                    "name"  => $photos["file_name"],
                    "uri"   => base_url("assets/dist/img/")
                ));
                $cover_id = $this->models->getMediaModel()->getID();
                // Prepare To Insert New News
                $new_news = array(
                    'title'             => $this->input->post('title'),
                    'content'           => $this->input->post('content'),
                    'uri'               => str_replace(' ', '-', strtolower($this->input->post('title'))) . '-' . time(),
                    'status'            => $this->input->post('status') ?: 1,
                    'cover_id'          => $cover_id ?: 2,
                    'user_id'           => $this->input->post('user_id') ?: 1,
                );

                // Create New News
                if ($this->models->getPostModel()->create($new_news)) {
                    $new_news_id = $this->models->getPostModel()->getID();
                    
                    if ($this->input->post("tags")) {
                        $tags = json_decode($this->input->post("tags"));
                        $tags = $tags->tags;
                        foreach ($tags as $item) {
                            $tag = $this->models->getTagModel()->getBy(array(
                                "label" => $item->item
                            ));
                            if (count($tag) > 0) {
                                $this->models->getTagPostModel()->create(array(
                                    "tag_id"    => $tag[0]["id"],
                                    "post_id"   => $new_news_id
                                ));
                            }
                            else {
                                $this->models->getTagModel()->create(array(
                                    "label"     => $item->item,
                                    "status"    => 1
                                ));
                                $tag_id = $this->models->getTagModel()->getID();
                                $this->models->getTagPostModel()->create(array(
                                    "tag_id"    => $tag_id,
                                    "post_id"   => $new_news_id
                                ));
                            }
                        }
                    }

                    $res = array(
                        'data'  => array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => "Berhasil membuat data berita"
                        )
                    );
                }
                else
                    $res = array(
                        'data'  => array(
                            'status'    => 'error',
                            'code'      => 5001,
                            'message'   => "Terjadi kesalahan pada berita"
                        )
                    );
            } else
                $res = array(
                    'data'  => array(
                        'status'    => 'error',
                        'code'      => 5002,
                        'message'   => "Terjadi kesalahan pada berita"
                    )
                );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Delete One News
    public function doDeleteNews()
    {

        // Find Specific news id From `m_posts` table
        $where = array(
            'm_posts.id' => $this->input->get('news_id')
        );

        // Cek if there is news id available
        if (!isset($_GET['news_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID news dibutuhkan'
                )
            );
        } else {
            // Delete news With Id
            if ($this->models->getPostModel()->delete($where)) {
                $res = array(
                    "data" => array(
                        "status"    => "success",
                        "code"      => 200,
                        "message"   => "Berhasil menghapus data berita"
                    )
                );
            } else {
                $res = array(
                    "data" => array(
                        "status"    => "error",
                        "code"      => 500,
                        "message"   => "Terjadi kesalahan berita tidak ditemukan"
                    )
                );
            }
        }
        // Return The Response
        echo json_encode($res);
    }

    // Update News
    public function doUpdateNews()
    {

        //Set Rule Validation
        $this->form_validation->set_rules('title', 'Title', 'required|max_length[255]');
        $this->form_validation->set_rules('content', 'Content', 'required');

        $where = array(
            'm_posts.id' => $this->input->post('news_id')
        );

        // Cek if there is id news available
        if (!isset($_POST['news_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID Berita dibutuhkan'
                )
            );
        } else {
            // Check Validation Rule
            if ($this->form_validation->run() == FALSE) {
                $res = array(
                    'data'  => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => validation_errors()
                    )
                );
            } else {
                $photos = $this->fileUpload->do_upload("photos");
                $cover_id = '';
                if ($photos["status"]) {
                    $this->models->getMediaModel()->create(array(
                        "name"  => $photos["file_name"],
                        "uri"   => base_url("assets/dist/img/")
                    ));
                    $cover_id = $this->models->getMediaModel()->getID();
                }

                $newsData = $this->models->getPostModel()->getBy($where)[0];

                // Prepare To Update News
                $news_update = array(
                    'title'             => $this->input->post('title') ?: $newsData['title'],
                    'content'           => $this->input->post('content') ?: $newsData['content'],
                    'uri'               => str_replace(' ', '-', strtolower($this->input->post('title'))) . '-' . time(),
                    'status'            => $this->input->post('status') ?: $newsData['status'],
                    'cover_id'          => $cover_id != '' ? $cover_id : $newsData['cover_id'],
                    'user_id'           => $this->input->post('user_id') ?: $newsData['user_id'],
                );
                // Update News
                if ($this->models->getPostModel()->update($where, $news_update)) {
                    
                    if ($this->input->post("tags")) {
                        $new_news_id = $this->input->post('news_id');
                        $tags = json_decode($this->input->post("tags"));
                        $tags = $tags->tags;

                        $this->models->getTagPostModel()->delete(array(
                            "post_id"   => $new_news_id
                        ));

                        foreach ($tags as $item) {
                            $tag = $this->models->getTagModel()->getBy(array(
                                "label" => $item->item
                            ));
                            if (count($tag) > 0) {
                                $tagPost = $this->models->getTagPostModel()->getBy(array(
                                    "tag_id"    => $tag[0]["id"],
                                    "post_id"   => $new_news_id
                                ));
                                if (count($tagPost) == 0) 
                                    $this->models->getTagPostModel()->create(array(
                                        "tag_id"    => $tag[0]["id"],
                                        "post_id"   => $new_news_id
                                    ));
                            }
                            else {
                                $this->models->getTagModel()->create(array(
                                    "label"     => $item->item,
                                    "status"    => 1
                                ));
                                $tag_id = $this->models->getTagModel()->getID();
                                $this->models->getTagPostModel()->create(array(
                                    "tag_id"    => $tag_id,
                                    "post_id"   => $new_news_id
                                ));
                            }
                        }
                    }

                    $res = array(
                        'data'  => array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => "Berhasil memperbarui data berita",
                            'data'      => $news_update
                        )
                    );
                }
                else
                    $res = array(
                        'data'  => array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => "Terjadi kesalahan pada berita",
                            'data'      => $news_update
                        )
                    );
            }
        }

        // Retrun The Response
        echo json_encode($res);
    }

    //Read All News
    public function doReadAllNews()
    {
        //Proccess Read All News
        $data = $this->models->getPostModel()->all();
        // Check The Result of Process Above
        if ($data) {
            $res = array(
                'data' => array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Berhasil mengambil data berita',
                    'data'      => $data
                )
            );
        } else {
            $res = array(
                'data' => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'Gagal mengambil data berita',
                    'data'      => []
                )
            );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    //Read One News
    public function doReadOnceNews()
    {
        // Find Specific id
        $where = array(
            'm_posts.id' => $this->input->get('post_id')
        );

        $data = $this->models->getPostModel()->getBy($where);
        // Check The Result
        if ($data) {
            $res = array(
                'data' => array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Berhasil mengambil data berita',
                    'data'      => $data
                )
            );
        } else {
            $res = array(
                'data' => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'Gagal mengambil data berita',
                    'data'      => []
                )
            );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Read Three Of Last News
    public function doReadThreeLastNews() 
    {
        $data = $this->models->getPostModel()->getJoinWithSelection(
            array(
                "m_posts.*"
            ),
            array(
                array(
                    "table" => "m_users u",
                    "on"    => "u.id=m_posts.user_id"
                )
            )
        );

        // Check The Result
        if ($data) {
            $res = array(
                'data' => array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Berhasil mengambil data berita',
                    'data'      => $data
                )
            );
        } else {
            $res = array(
                'data' => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'Gagal mengambil data berita',
                    'data'      => []
                )
            );
        }

        // Retrun The Response
        echo json_encode($res);
    }
    // -------------------------------------------------------------------------------------- END OF NEWS ACTION

    // -------------------------------------------------------------------------------------- MENU ACTION START
    // Create Menu
    public function doCreateMenu()
    {
        // Set Rule Validation
        $this->form_validation->set_rules('label', 'Label', 'required|max_length[100]');
        $this->form_validation->set_rules('link', 'Link', 'required|max_length[255]');
        $this->form_validation->set_rules('icon', 'Icon', 'max_length[100]');
        $this->form_validation->set_rules('class', 'Class', 'max_length[100]');

        // Check Validation Rule
        if ($this->form_validation->run() == FALSE) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => validation_errors()
                )
            );
        } else {
            // Prepare To Create New Menu
            $new_menu = array(
                'label'             => $this->input->post('label'),
                'link'              => $this->input->post('link'),
                'icon'              => $this->input->post('icon') ?: '',
                'class'             => $this->input->post('class') ?: '',
                'parent_id'         => $this->input->post('parent_id') ?: null,
                'status'            => $this->input->post('status') ?: 1,
            );
            // Create New Menu
            if ($this->models->getMenuModel()->create($new_menu))
                $res = array(
                    'data'  => array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => "Berhasil membuat data menu"
                    )
                );
            else
                $res = array(
                    'data'  => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => "Terjadi kesalahan pada menu"
                    )
                );
        }
        // Retrun The Response
        echo json_encode($res);
    }


    // Read All Menu
    public function doReadAllMenu()
    {
        $selection =  array(
            'm_menus.id menu_id',
            'm_menus.label',
            'm_menus.link',
            'm_menus.icon',
            'm_menus.class',
            'm_menus.parent_id',
            'm.label parent_label'
        );

        // Join Description
        $join = array(
            'table' => array(
                'column'    => 'm_menus m',
                'on'        => 'm_menus.parent_id = m.id'
            )
        );

        // Process Select All Menus With Join to Parent Menu
        $data = $this->models->getMenuModel()->getMenusWithJoin($selection, $join);

        // Check The Result of Process Above
        if ($data) {
            $res = array(
                'data' => array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Berhasil mengambil data menu',
                    'data'      => $data
                )
            );
        } else {
            $res = array(
                'data' => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'Gagal mengambil data menu',
                    'data'      => []
                )
            );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Read One Menu
    public function doReadOnceMenu()
    {

        $selection =  array(
            'm_menus.id menu_id',
            'm_menus.label',
            'm_menus.link',
            'm_menus.icon',
            'm_menus.class',
            'm_menus.parent_id',
            'm.label parent_label'
        );

        $join = array(
            'table' => array(
                'column'    => 'm_menus m',
                'on'        => 'm_menus.parent_id = m.id'
            )
        );

        // Find Specific menu id From `m_menus` table
        $where = array(
            'm_menus.id' => $this->input->get('menu_id')
        );

        // Cek if there is id menu available
        if (!isset($_GET['menu_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID menu dibutuhkan'
                )
            );
        } else {
            // Process Select Specific Menus With Join to Parent Menu
            $data = $this->models->getMenuModel()->getMenuWithJoinAndWhere($selection, $join, $where);
            if ($data) {
                $res = array(
                    'data' => array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Berhasil mengambil data menu',
                        'data'      => $data
                    )
                );
            } else {
                $res = array(
                    'data' => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'Gagal mengambil data menu',
                        'data'      => []
                    )
                );
            }
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Delete One User
    public function doDeleteMenu()
    {

        // Find Specific menu id From `m_menus` table
        $where = array(
            'm_menus.id' => $this->input->get('menu_id')
        );

        // Cek if there is menu id available
        if (!isset($_GET['menu_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID menu dibutuhkan'
                )
            );
        } else {
            // Delete Menu With Id
            if ($this->models->getMenuModel()->delete($where)) {
                $res = array(
                    "data" => array(
                        "status"    => "success",
                        "code"      => 200,
                        "message"   => "Berhasil menghapus data menu"
                    )
                );
            } else {
                $res = array(
                    "data" => array(
                        "status"    => "error",
                        "code"      => 500,
                        "message"   => "Terjadi kesalahan menu tidak ditemukan"
                    )
                );
            }
        }
        // Return The Response
        echo json_encode($res);
    }

    // Update User
    public function doUpdateMenu()
    {

        // Set Rule Validation
        $this->form_validation->set_rules('menu_id', 'ID Menu', 'required');

        // If admin update menu label, label must be below 100 char and required
        $this->form_validation->set_rules('label', 'Label', 'required|max_length[100]');

        // If admin update menu icon, icon must be below 100 char
        $this->form_validation->set_rules('icon', 'Icon', 'max_length[100]');

        // If admin update menu class, class must be below 100 char
        $this->form_validation->set_rules('class', 'Class', 'max_length[100]');

        $where = array(
            'm_menus.id' => $this->input->post('menu_id')
        );

        // Cek if there is menu id available
        if (!isset($_POST['menu_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID menu dibutuhkan'
                )
            );
        } else {
            // Check Validation Rule
            if ($this->form_validation->run() == FALSE) {
                $res = array(
                    'data'  => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => validation_errors()
                    )
                );
            } else {
                $menuData = $this->models->getMenuModel()->getBy($where)[0];

                // Prepare To Update Menu
                $menu_update = array(
                    'id'                => $this->input->post('menu_id') ?: $menuData['id'],
                    'label'             => $this->input->post('label') ?: $menuData['label'],
                    'link'              => $this->input->post('link') ?: $menuData['link'],
                    'icon'              => $this->input->post('icon') ?: $menuData['icon'],
                    'class'             => $this->input->post('class') ?: $menuData['icon'],
                    'parent_id'         => $this->input->post('parent_id') ?: $menuData['parent_id'],
                    'status'            => $this->input->post('status') ?: $menuData['status']
                );
                // Update menu
                if ($this->models->getMenuModel()->update($where, $menu_update))
                    $res = array(
                        'data'  => array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => "Berhasil memperbarui data menu"
                        )
                    );
                else
                    $res = array(
                        'data'  => array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => "Terjadi kesalahan pada menu"
                        )
                    );
            }
        }

        // Retrun The Response
        echo json_encode($res);
    }
    // -------------------------------------------------------------------------------------- END OF MENU ACTION

    // -------------------------------------------------------------------------------------- PAGE ACTION START
    // Create Page
    public function doCreatePage()
    {
        //Set Rule Validation
        $this->form_validation->set_rules('title', 'Title', 'required|max_length[255]');
        $this->form_validation->set_rules('content', 'Content', 'required');

        if ($this->form_validation->run() == FALSE) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => validation_errors()
                )
            );
        } else {
            // Insert Photo First
            $photos = $this->fileUpload->do_upload("photos");
            if ($photos["status"]) {
                $this->models->getMediaModel()->create(array(
                    "name"  => $photos["file_name"],
                    "uri"   => base_url("assets/dist/img/")
                ));
                $cover_id = $this->models->getMediaModel()->getID();
            }
            else
                $cover_id = null;

                // Prepare To Create New Page
                $new_page = array(
                    'title'             => $this->input->post('title'),
                    'content'           => $this->input->post('content'),
                    'uri'               => str_replace(' ', '-', strtolower($this->input->post('title'))) . '-' . time(),
                    'status'            => $this->input->post('status') ?: 1,
                    'cover_id'          => $cover_id,
                    'user_id'           => $this->input->post('user_id') ?: 1,
                );

                // Create New Page
                if ($this->models->getPageModel()->create($new_page)) {
                    $new_page_id = $this->models->getPageModel()->getID();
                    
                    if ($this->input->post("tags")) {
                        $tags = json_decode($this->input->post("tags"));
                        $tags = $tags->tags;
                        foreach ($tags as $item) {
                            $tag = $this->models->getTagModel()->getBy(array(
                                "label" => $item->item
                            ));
                            if (count($tag) > 0) {
                                $this->models->getTagPageModel()->create(array(
                                    "tag_id"    => $tag[0]["id"],
                                    "page_id"   => $new_page_id
                                ));
                            }
                            else {
                                $this->models->getTagModel()->create(array(
                                    "label"     => $item->item,
                                    "status"    => 1
                                ));
                                $tag_id = $this->models->getTagModel()->getID();
                                
                                $this->models->getTagPageModel()->create(array(
                                    "tag_id"    => $tag_id,
                                    "page_id"   => $new_page_id
                                ));
                            }
                        }
                    }

                    $res = array(
                        'data'  => array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => "Berhasil membuat data halaman"
                        )
                    );
                }
                else
                    $res = array(
                        'data'  => array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => "Terjadi kesalahan pada halaman"
                        )
                    );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Update Pages
    public function doUpdatePages()
    {

        //Set Rule Validation
        $this->form_validation->set_rules('title', 'Title', 'required|max_length[255]');
        $this->form_validation->set_rules('content', 'Content', 'required');

        $where = array(
            'm_pages.id' => $this->input->post('page_id')
        );

        // Cek if there is id pages available
        if (!isset($_POST['page_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID page dibutuhkan'
                )
            );
        } else {
            // Check Validation Rule
            if ($this->form_validation->run() == FALSE) {
                $res = array(
                    'data'  => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => validation_errors()
                    )
                );
            } else {
                $photos = $this->fileUpload->do_upload("photos");
                $cover_id = '';
                if ($photos["status"]) {
                    $this->models->getMediaModel()->create(array(
                        "name"  => $photos["file_name"],
                        "uri"   => base_url("assets/dist/img/")
                    ));
                    $cover_id = $this->models->getMediaModel()->getID();
                }

                $pageData = $this->models->getPageModel()->getBy($where)[0];

                // Prepare To Update Pages
                $pages_update = array(
                    'title'             => $this->input->post('title') ?: $pageData['title'],
                    'content'           => $this->input->post('content') ?: $pageData['content'],
                    'uri'               => str_replace(' ', '-', strtolower($this->input->post('title'))) . '-' . time(),
                    'status'            => $this->input->post('status') ?: $pageData['status'],
                    'cover_id'          => $cover_id != '' ? $cover_id : $pageData['cover_id'],
                    'user_id'           => $this->input->post('user_id') ?: $pageData['user_id'],
                );
                // Update Pages
                if ($this->models->getPageModel()->update($where, $pages_update)) {

                    if ($this->input->post("tags")) {
                        $new_page_id = $this->input->post('page_id');
                        $tags = json_decode($this->input->post("tags"));
                        $tags = $tags->tags;

                        $this->models->getTagPageModel()->delete(array(
                            "page_id"   => $new_page_id
                        ));

                        foreach ($tags as $item) {
                            $tag = $this->models->getTagModel()->getBy(array(
                                "label" => $item->item
                            ));
                            if (count($tag) > 0) {
                                $tagPost = $this->models->getTagPageModel()->getBy(array(
                                    "tag_id"    => $tag[0]["id"],
                                    "page_id"   => $new_page_id
                                ));
                                if (count($tagPost) == 0) 
                                    $this->models->getTagPageModel()->create(array(
                                        "tag_id"    => $tag[0]["id"],
                                        "page_id"   => $new_page_id
                                    ));
                            }
                            else {
                                $this->models->getTagModel()->create(array(
                                    "label"     => $item->item,
                                    "status"    => 1
                                ));
                                $tag_id = $this->models->getTagModel()->getID();
                                $this->models->getTagPageModel()->create(array(
                                    "tag_id"    => $tag_id,
                                    "page_id"   => $new_page_id
                                ));
                            }
                        }
                    }

                    $res = array(
                        'data'  => array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => "Berhasil memperbarui data halaman",
                            'data'      => $pages_update
                        )
                    );
                }
                else
                    $res = array(
                        'data'  => array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => "Terjadi kesalahan pada halaman",
                            'data'      => $pages_update
                        )
                    );
            }
        }

        // Retrun The Response
        echo json_encode($res);
    }

    // Delete One Page
    public function doDeletePage()
    {

        // Find Specific page id From `m_pages` table
        $where = array(
            'm_pages.id' => $this->input->get('page_id')
        );

        // Cek if there is page id available
        if (!isset($_GET['page_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID halaman dibutuhkan'
                )
            );
        } else {
            // Delete Page With Id
            if ($this->models->getPageModel()->delete($where)) {
                $res = array(
                    "data" => array(
                        "status"    => "success",
                        "code"      => 200,
                        "message"   => "Berhasil menghapus data halaman"
                    )
                );
            } else {
                $res = array(
                    "data" => array(
                        "status"    => "error",
                        "code"      => 500,
                        "message"   => "Terjadi kesalahan halaman tidak ditemukan"
                    )
                );
            }
        }
        // Return The Response
        echo json_encode($res);
    }

    // Read One Page
    public function doReadPage()
    {
        $selection =  array(
            'id page_id',
            'title',
            'uri',
            'content',
            'status',
            'cover_id',
            'user_id',
        );

        // Find Specific page id From `m_pages` table
        $where = array(
            'm_pages.id' => $this->input->get('page_id')
        );

        // Cek if there is page id available
        if (!isset($_GET['page_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID page dibutuhkan'
                )
            );
        } else {
            // Process Select Specific Page
            $data = $this->models->getPageModel()->getWithSelectionAndWhere($selection, $where);
            if ($data)
                $res = array(
                    'data' => array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Berhasil mengambil data halaman',
                        'data'      => $data
                    )
                );
            else
                $res = array(
                    'data' => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'Gagal mengambil data halaman',
                        'data'      => []
                    )
                );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Read All Pages
    public function doReadAllPages()
    {
        $selection =  array(
            'id page_id',
            'title',
            'uri',
            'content',
            'status',
            'cover_id',
            'user_id',
        );

        // Process Select All Pages
        $data = $this->models->getPageModel()->getWithSelection($selection);
        if ($data)
            $res = array(
                'data' => array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Berhasil mengambil data halaman',
                    'data'      => $data
                )
            );
        else
            $res = array(
                'data' => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'Gagal mengambil data halaman',
                    'data'      => []
                )
            );

        // Retrun The Response
        echo json_encode($res);
    }

    // Counter Dashboard
    public function doCounterDashboard()
    {
        //Proccess Counter
        $data = array(
            'posts_count'    => $this->models->getPostModel()->count(),
            'pages_count'    => $this->models->getPageModel()->count(),
            'users_count'    => $this->models->getUserModel()->count(),
            'comments_count' => $this->models->getCommentModel()->count()
        );

        // Check The Result
        if ($data) {
            $res = array(
                'data' => array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Berhasil mengambil data counter',
                    'data'      => $data
                )
            );
        } else {
            $res = array(
                'data' => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'Gagal mengambil data counter',
                    'data'      => []
                )
            );
        }
        // Retrun The Response
        echo json_encode($res);
    }
    // -------------------------------------------------------------------------------------- END OF PAGE ACTION

    // -------------------------------------------------------------------------------------- TAG ACTION START
    // Create Tag
    public function doCreateTag()
    {
        // Set Rule Validation
        $this->form_validation->set_rules('label', 'Label', 'required|max_length[100]');
        // Check Validation Rule
        if ($this->form_validation->run() == FALSE)
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => validation_errors()
                )
            );
        else {
            // Prepare To Create New tag
            $new_tag = array(
                'label'             => $this->input->post('label'),
                'status'           => $this->input->post('status') ?: 1
            );

            // Create New tag
            if ($this->models->getTagModel()->create($new_tag))
                $res = array(
                    'data'  => array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => "Berhasil membuat data tag"
                    )
                );
            else
                $res = array(
                    'data'  => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => "Terjadi kesalahan pada tag"
                    )
                );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Read All Tags
    public function doReadAllTag()
    {
        $selection =  array(
            'id tag_id',
            'label',
            'status'
        );

        // Process Select All Tag
        $data = $this->models->getTagModel()->getWithSelection($selection);
        if ($data)
            $res = array(
                'data' => array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Berhasil mengambil data tag',
                    'data'      => $data
                )
            );
        else
            $res = array(
                'data' => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'Gagal mengambil data tag',
                    'data'      => []
                )
            );

        // Retrun The Response
        echo json_encode($res);
    }

    // Read One Tag
    public function doReadOnceTag()
    {
        $selection =  array(
            'id tag_id',
            'label',
            'status'
        );

        // Find Specific tag id From `m_tags` table
        $where = array(
            'm_tags.id' => $this->input->get('tag_id')
        );

        // Cek if there is tag id available
        if (!isset($_GET['tag_id']))
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID tag dibutuhkan'
                )
            );
        else {
            // Process Select Specific tag
            $data = $this->models->getTagModel()->getWithSelectionAndWhere($selection, $where);
            if ($data)
                $res = array(
                    'data' => array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Berhasil mengambil data tag',
                        'data'      => $data
                    )
                );
            else
                $res = array(
                    'data' => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'Gagal mengambil data tag',
                        'data'      => []
                    )
                );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Delete One Tag
    public function doDeleteTag()
    {

        // Find Specific tag id From `m_tags` table
        $where = array(
            'm_tags.id' => $this->input->get('tag_id')
        );

        // Cek if there is tag id available
        if (!isset($_GET['tag_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID tag dibutuhkan'
                )
            );
        } else {
            // Delete Department With Id
            if ($this->models->getTagModel()->delete($where)) {
                $res = array(
                    "data" => array(
                        "status"    => "success",
                        "code"      => 200,
                        "message"   => "Berhasil menghapus data tag"
                    )
                );
            } else {
                $res = array(
                    "data" => array(
                        "status"    => "error",
                        "code"      => 500,
                        "message"   => "Terjadi kesalahan tag tidak ditemukan"
                    )
                );
            }
        }
        // Return The Response
        echo json_encode($res);
    }

    // Update Tag
    public function doUpdateTag()
    {

        // If admin update tag name, name must be below 100 char and required
        $this->form_validation->set_rules('label', 'Label', 'required|max_length[100]');

        $where = array(
            'm_tags.id' => $this->input->post('tag_id')
        );

        // Cek if there is tag id available
        if (!isset($_POST['tag_id']))
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID tag dibutuhkan'
                )
            );
        else {
            // Check Validation Rule
            if ($this->form_validation->run() == FALSE)
                $res = array(
                    'data'  => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => validation_errors()
                    )
                );
            else {
                $tagData = $this->models->getTagModel()->getBy($where)[0];

                // Prepare To Update Tag
                $tag_update = array(
                    'label'               => $this->input->post('label') ?: $tagData['label'],
                    'status'              => $this->input->post('status') ?: $tagData['status']
                );

                // Update Tag
                if ($this->models->getTagModel()->update($where, $tag_update))
                    $res = array(
                        'data'  => array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => "Berhasil memperbarui data tag"
                        )
                    );
                else
                    $res = array(
                        'data'  => array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => "Terjadi kesalahan pada tag"
                        )
                    );
            }
        }

        // Retrun The Response
        echo json_encode($res);
    }
    // -------------------------------------------------------------------------------------- END OF TAG ACTION

    // -------------------------------------------------------------------------------------- DEPARTMENT ACTION START
    // Create Department
    public function doCreateDepartment()
    {
        // Set Rule Validation
        $this->form_validation->set_rules('name', 'Name', 'required|max_length[100]');

        // Check Validation Rule
        if ($this->form_validation->run() == FALSE)
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => validation_errors()
                )
            );
        else {
            // Prepare To Create New Department
            $new_department = array(
                'name'             => $this->input->post('name'),
                'status'           => $this->input->post('status') ?: 1,
            );

            // Create New Department
            if ($this->models->getDepartmentModel()->create($new_department))
                $res = array(
                    'data'  => array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => "Berhasil membuat data jabatan"
                    )
                );
            else
                $res = array(
                    'data'  => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => "Terjadi kesalahan pada jabatan"
                    )
                );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Read All Departments
    public function doReadAllDepartment()
    {
        $selection =  array(
            'id department_id',
            'name',
            'status'
        );

        // Process Select All Department
        $data = $this->models->getDepartmentModel()->getWithSelection($selection);

        // Check The Result of Process Above
        if ($data)
            $res = array(
                'data' => array(
                    'status'    => 'success',
                    'code'      => 200,
                    'message'   => 'Berhasil mengambil data jabatan',
                    'data'      => $data
                )
            );
        else
            $res = array(
                'data' => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'Gagal mengambil data jabatan',
                    'data'      => []
                )
            );

        // Retrun The Response
        echo json_encode($res);
    }

    // Read One Department
    public function doReadOnceDepartment()
    {
        $selection =  array(
            'id department_id',
            'name',
            'status'
        );

        // Find Specific department id From `m_departments` table
        $where = array(
            'm_departments.id' => $this->input->get('department_id')
        );

        // Cek if there is department id available
        if (!isset($_GET['department_id']))
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID department dibutuhkan'
                )
            );
        else {
            // Process Select Specific Department
            $data = $this->models->getDepartmentModel()->getWithSelectionAndWhere($selection, $where);
            if ($data)
                $res = array(
                    'data' => array(
                        'status'    => 'success',
                        'code'      => 200,
                        'message'   => 'Berhasil mengambil data jabatan',
                        'data'      => $data
                    )
                );
            else
                $res = array(
                    'data' => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => 'Gagal mengambil data jabatan',
                        'data'      => []
                    )
                );
        }
        // Retrun The Response
        echo json_encode($res);
    }

    // Delete One Department
    public function doDeleteDepartment()
    {

        // Find Specific department id From `m_departments` table
        $where = array(
            'm_departments.id' => $this->input->get('department_id')
        );

        // Cek if there is department id available
        if (!isset($_GET['department_id'])) {
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID department dibutuhkan'
                )
            );
        } else {
            // Delete Department With Id
            if ($this->models->getDepartmentModel()->delete($where)) {
                $res = array(
                    "data" => array(
                        "status"    => "success",
                        "code"      => 200,
                        "message"   => "Berhasil menghapus data jabatan"
                    )
                );
            } else {
                $res = array(
                    "data" => array(
                        "status"    => "error",
                        "code"      => 500,
                        "message"   => "Terjadi kesalahan jabatan tidak ditemukan"
                    )
                );
            }
        }
        // Return The Response
        echo json_encode($res);
    }

    // Update Department
    public function doUpdateDepartment()
    {

        // If admin update department name, name must be below 100 char and required
        $this->form_validation->set_rules('name', 'Name', 'required|max_length[100]');

        $where = array(
            'm_departments.id' => $this->input->post('department_id')
        );

        // Cek if there is department id available
        if (!isset($_POST['department_id']))
            $res = array(
                'data'  => array(
                    'status'    => 'error',
                    'code'      => 500,
                    'message'   => 'ID department dibutuhkan'
                )
            );
        else {
            // Check Validation Rule
            if ($this->form_validation->run() == FALSE)
                $res = array(
                    'data'  => array(
                        'status'    => 'error',
                        'code'      => 500,
                        'message'   => validation_errors()
                    )
                );
            else {
                $departmentData = $this->models->getDepartmentModel()->getBy($where)[0];

                // Prepare To Update Department
                $department_update = array(
                    'name'              => $this->input->post('name') ?: $departmentData['name'],
                    'status'            => $this->input->post('status') ?: $departmentData['status']
                );

                // Update department
                if ($this->models->getDepartmentModel()->update($where, $department_update))
                    $res = array(
                        'data'  => array(
                            'status'    => 'success',
                            'code'      => 200,
                            'message'   => "Berhasil memperbarui data jabatan"
                        )
                    );
                else
                    $res = array(
                        'data'  => array(
                            'status'    => 'error',
                            'code'      => 500,
                            'message'   => "Terjadi kesalahan pada jabatan"
                        )
                    );
            }
        }

        // Retrun The Response
        echo json_encode($res);
    }
    // -------------------------------------------------------------------------------------- END OF DEPARTMENT ACTION
}
