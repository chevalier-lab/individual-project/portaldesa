<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerAuth extends CI_Controller {
    private $models, $session;
    public function __construct($models) {
        // Load All Model
        $this->models = $models;
        parent::__construct();
        $this->session = new Session_helper();
    }

    // Check Authentication
    public function checkAuth() {
        $auth = array(
            'api_key'       => $this->input->post_get('api_token', TRUE),
            'email'         => $this->input->post_get('email', TRUE),
            'username'      => $this->input->post_get('username', TRUE),
            'password'      => $this->input->post_get('password', TRUE),
        );
        if (isset($auth['api_key'])) {
            $res = $this->models->getUserModel()->getBy(
                array(
                    'api_key'   => $auth['api_key']
                )
            );
        }
        else if (isset($auth['email']) && isset($auth['password'])) {
            $res = $this->models->getUserModel()->getBy(
                array(
                    'email'     => $auth['email'],
                    'password'     => md5($auth['password'])
                )
            );
        }
        else {
            $res = $this->models->getUserModel()->getBy(
                array(
                    'username'     => $auth['username'],
                    'password'     => md5($auth['password'])
                )
            );
        }
        return !empty($res);
    }

    // Get Auth
    public function auth() {
        $auth = array(
            'api_key'       => $this->input->post_get('api_token', TRUE),
            'email'         => $this->input->post_get('email', TRUE),
            'username'      => $this->input->post_get('username', TRUE),
            'password'      => $this->input->post_get('password', TRUE),
        );
        
        if (isset($auth['api_key'])) {
            $res = $this->models->getUserModel()->getBy(
                array(
                    'api_key'   => $auth['api_key']
                )
            );
        }
        else if (isset($auth['email']) && isset($auth['password'])) {
            $res = $this->models->getUserModel()->getBy(
                array(
                    'email'     => $auth['email'],
                    'password'     => md5($auth['password'])
                )
            );
        }
        else {
            $res = $this->models->getUserModel()->getBy(
                array(
                    'username'     => $auth['username'],
                    'password'     => md5($auth['password'])
                )
            );
        }
        return !empty($res) ? $res : null;
    }

    // Create Session
    public function createSession($auth=array()) {
        if (count($auth) > 0) {
            $data_session = array(
                'auth'      => $auth,
                'status'    => true
            );
            $this->session->add_session('authentication', $data_session);
            return true;
        }
        return false;
    }

    // Get Session
    public function getSession() {
        if ($this->session->check_session("authentication")) 
            return $this->session->check_session("authentication");
        return null;
    }

    // Destroy Session
    public function destroySession() {
        $this->session->remove_session('authentication');
        $this->session->destroy_session('authentication');
        return true;
    }
}