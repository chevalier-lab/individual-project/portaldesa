<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TagPostModel extends CI_Model {
    private $table;
    public function __construct() {
        $this->table = 't_tags_posts';
    }

    // Create Tag
    public function create($data=array()) {
        if (count($data) > 0)
            $this->db->insert($this->table, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Read Tag (All)
    public function all() {
        $this->db->order_by('id', 'DESC');
        return $this->db->get($this->table)->result_array();
    }

    // Read Tag By
    public function getBy($where=array()) {
        $this->db->where($where);
        return $this->all();
    }

    // Read Tag All With Selection
    public function getWithSelection($selection=array()) {
        if (count($selection) > 0) 
            $this->db->select(implode(', ', $selection));
        return $this->all();
    }

    // Read Tag All With Selection And Where
    public function getWithSelectionAndWhere($selection=array(), $where=array()) {
        if (count($selection) > 0) 
            $this->db->select(implode(', ', $selection));
        return $this->getBy($where);
    }

    // Update Tag
    public function update($where=array(), $data=array()) {
        $this->db->where($where);
        if (count($data) > 0)
            $this->db->update($this->table, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Delete Tag
    public function delete($where=array()) {
        $this->db->where($where);
        $this->db->delete($this->table);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Read Tag With 1 Join
    public function getTagPostWithWhereJoin($selection = array(), $join = array(), $where = array())
    {
        // Inital Variable
        $table1 = $join['table1'];
        $table2 = $join['table2'];

        if (count($selection) > 0) {
            $this->db->select(implode(', ', $selection['select']));
            $this->db->join($table1['column'], $table1['on']);
            $this->db->join($table2['column'], $table2['on']);
        }
        $this->db->where($where);
        return $this->db->get($this->table)->result_array();
    }
}