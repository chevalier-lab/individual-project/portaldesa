<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MenuModel extends CI_Model
{
    private $table;
    public function __construct()
    {
        $this->table = 'm_menus';
    }

    // Create Menu
    public function create($data = array())
    {
        if (count($data) > 0)
            $this->db->insert($this->table, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Read Menu (All)
    public function all()
    {
        $this->db->order_by('m_menus.id', 'DESC');
        return $this->db->get($this->table)->result_array();
    }

    // Read Menu By
    public function getBy($where = array())
    {
        $this->db->where($where);
        return $this->all();
    }

    // Read Menu All With Selection
    public function getWithSelection($selection = array())
    {
        if (count($selection) > 0)
            $this->db->select(implode(', ', $selection));
        return $this->all();
    }

    // Read Menu All With Selection And Where
    public function getWithSelectionAndWhere($selection = array(), $where = array())
    {
        if (count($selection) > 0)
            $this->db->select(implode(', ', $selection));
        return $this->getBy($where);
    }

    // Read All Menus With Join
    public function getMenusWithJoin($selection = array(), $join = array())
    {
        // Inital Variable
        $table = $join['table'];

        if (count($selection) > 0) {
            $this->db->select(implode(', ', $selection));
            $this->db->join($table['column'], $table['on'], 'left');
        }
        return $this->all();
    }

    // Read Specific Menu With Join
    public function getMenuWithJoinAndWhere($selection = array(), $join = array(), $where = array())
    {
        // Inital Variable
        $table = $join['table'];

        if (count($selection) > 0) {
            $this->db->select(implode(', ', $selection));
            $this->db->join($table['column'], $table['on'], 'left');
        }
        return $this->getBy($where);
    }

    // Update Menu
    public function update($where = array(), $data = array())
    {
        $this->db->where($where);
        if (count($data) > 0)
            $this->db->update($this->table, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Delete Menu
    public function delete($where = array())
    {
        $this->db->where($where);
        $this->db->delete($this->table);
        return ($this->db->affected_rows() != 1) ? false : true;
    }
}
