<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PageModel extends CI_Model {
    private $table;
    public function __construct() {
        $this->table = 'm_pages';
    }

    // Get ID From Insert
    public function getID()
    {
        return $this->db->insert_id();
    }

    // Create Page
    public function create($data=array()) {
        if (count($data) > 0)
            $this->db->insert($this->table, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Read Page (All)
    public function all() {
        $this->db->order_by('id', 'DESC');
        return $this->db->get($this->table)->result_array();
    }

    // Read Page By
    public function getBy($where=array()) {
        $this->db->where($where);
        return $this->all();
    }

    // Read Page All With Selection
    public function getWithSelection($selection=array()) {
        if (count($selection) > 0) 
            $this->db->select(implode(', ', $selection));
        return $this->all();
    }

    // Read Page All With Selection And Where
    public function getWithSelectionAndWhere($selection=array(), $where=array()) {
        if (count($selection) > 0) 
            $this->db->select(implode(', ', $selection));
        return $this->getBy($where);
    }

    // Update Page
    public function update($where=array(), $data=array()) {
        $this->db->where($where);
        if (count($data) > 0)
            $this->db->update($this->table, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Delete Page
    public function delete($where=array()) {
        $this->db->where($where);
        $this->db->delete($this->table);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Count Page
    public function count(){
        return $this->db->count_all($this->table);
    }

    // Read Page With 1 Join
    public function getPageWithStatus($selection = array(), $join = array(), $where = array()) {
        // Inital Variable
        $table1 = $join['table1'];

        if (count($selection) > 0) {
            $this->db->select(implode(', ', $selection['select']));
            $this->db->join($table1['column'], $table1['on'], $table1['direction']);
        }
        return $this->getBy($where);
    }

    // Get Join 
    public function getJoin($join=array()) {
        foreach ($join as $itemJoin) {
            $this->db->join($itemJoin["table"], $itemJoin["on"], $itemJoin["direction"]);
        }
        return $this->all();
    }

    // Get Join With Where
    public function getJoinWhere($join=array(), $where=array()) {
        $this->db->where($where);
        return $this->getJoin($join);
    }

    // Get Join With Selection
    public function getJoinWithSelection($selection=array(), $join=array()) {
        if (count($selection) > 0) 
            $this->db->select(implode(', ', $selection));
        return $this->getJoin($join);
    }

    // Get Join With Selection And Where
    public function getJoinWithSelectionAndWhere($selection=array(), $join=array(), $where=array()) {
        if (count($selection) > 0) 
            $this->db->select(implode(', ', $selection));
        return $this->getJoinWhere($join, $where);
    }
}