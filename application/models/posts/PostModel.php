<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PostModel extends CI_Model {
    private $table;
    public function __construct() {
        $this->table = 'm_posts';
    }

    // Create Post
    public function create($data=array()) {
        if (count($data) > 0)
            $this->db->insert($this->table, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Get ID From Insert
    public function getID()
    {
        return $this->db->insert_id();
    }

    // Read Post (All)
    public function all() {
        $this->db->order_by('id', 'DESC');
        return $this->db->get($this->table)->result_array();
    }

    // Read Post By
    public function getBy($where=array()) {
        $this->db->where($where);
        return $this->all();
    }

    // Read Post All With Selection
    public function getWithSelection($selection=array()) {
        if (count($selection) > 0) 
            $this->db->select(implode(', ', $selection));
        return $this->all();
    }

    // Read Post All With Selection And Where
    public function getWithSelectionAndWhere($selection=array(), $where=array()) {
        if (count($selection) > 0) 
            $this->db->select(implode(', ', $selection));
        return $this->getBy($where);
    }

    // Update Post
    public function update($where=array(), $data=array()) {
        $this->db->where($where);
        if (count($data) > 0)
            $this->db->update($this->table, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Delete Post
    public function delete($where=array()) {
        $this->db->where($where);
        $this->db->delete($this->table);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Count Post
    public function count(){
        return $this->db->count_all($this->table);
    }

    // Read Post With 1 Join
    public function getPostWithStatus($selection = array(), $join = array(), $where = array())
    {
        // Inital Variable
        $table1 = $join['table1'];

        if (count($selection) > 0) {
            $this->db->select(implode(', ', $selection['select']));
            $this->db->join($table1['column'], $table1['on']);
        }
        return $this->getBy($where);
    }

    // Get Join 
    public function getJoin($join=array()) {
        foreach ($join as $itemJoin) {
            $this->db->join($itemJoin["table"], $itemJoin["on"]);
        }
        return $this->all();
    }

    // Get Join With Where
    public function getJoinWhere($join=array(), $where=array()) {
        $this->db->where($where);
        return $this->getJoin($join);
    }

    // Get Join With Selection
    public function getJoinWithSelection($selection=array(), $join=array()) {
        if (count($selection) > 0) 
            $this->db->select(implode(', ', $selection));
        return $this->getJoin($join);
    }

    // Get Join With Selection And Where
    public function getJoinWithSelectionAndWhere($selection=array(), $join=array(), $where=array()) {
        if (count($selection) > 0) 
            $this->db->select(implode(', ', $selection));
        return $this->getJoinWhere($join, $where);
    }
}