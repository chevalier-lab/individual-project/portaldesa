<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserModel extends CI_Model
{
    private $table;
    public function __construct()
    {
        $this->table = 'm_users';
    }

    // Create User
    public function create($data = array())
    {
        if (count($data) > 0)
            $this->db->insert($this->table, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Get ID From Insert
    public function getID()
    {
        return $this->db->insert_id();
    }

    // Read User (All)
    public function all()
    {
        $this->db->order_by('m_users.id', 'DESC');
        return $this->db->get($this->table)->result_array();
    }

    // Read User By
    public function getBy($where = array())
    {
        $this->db->where($where);
        return $this->all();
    }

    // Read User All With Selection
    public function getWithSelection($selection = array())
    {
        if (count($selection) > 0)
            $this->db->select(implode(', ', $selection));
        return $this->all();
    }

    // Read User All With Selection And Where
    public function getWithSelectionAndWhere($selection = array(), $where = array())
    {
        if (count($selection) > 0)
            $this->db->select(implode(', ', $selection));
        return $this->getBy($where);
    }

    // Read All User With Join department
    public function getUsersWithStatus($selection = array(), $join = array())
    {
        // Inital Variable
        $table1 = $join['table1'];
        $table2 = $join['table2'];

        if (count($selection) > 0) {
            $this->db->select(implode(', ', $selection['select']));
            $this->db->join($table1['column'], $table1['on']);
            $this->db->join($table2['column'], $table2['on']);

        }
        return $this->all();
    }

    // Read All User With Join department
    public function getUserWithStatus($selection = array(), $join = array(), $where = array())
    {
        // Inital Variable
        $table1 = $join['table1'];
        $table2 = $join['table2'];
        $table3 = $join['table3'];

        if (count($selection) > 0) {
            $this->db->select(implode(', ', $selection['select']));
            $this->db->join($table1['column'], $table1['on']);
            $this->db->join($table2['column'], $table2['on']);
            $this->db->join($table3['column'], $table3['on']);
        }
        return $this->getBy($where);
    }

    // Update User
    public function update($where = array(), $data = array())
    {
        $this->db->where($where);
        if (count($data) > 0)
            $this->db->update($this->table, $data);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Delete User
    public function delete($where = array())
    {
        $this->db->where($where);
        $this->db->delete($this->table);
        return ($this->db->affected_rows() != 1) ? false : true;
    }

    // Count User
    public function count(){
        return $this->db->count_all($this->table);
    }
}
