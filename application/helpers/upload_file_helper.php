<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_file_helper {
    protected $config;
    protected $file_dir;

    public function __construct($config) {
        $this->config = $config;
        $this->file_dir = FCPATH . "assets/dist/img/";
    }

    // Check Size
    private function checkSize($upload_key='foo') {
        $defaultSize = (isset($this->config['max_size'])) ? $this->config['max_size'] : 500000;
        return ($_FILES[$upload_key]['size'] <= $defaultSize);
    }

    // Check File Tipe
    private function checkFileType($upload_key='foo') {
        $target_file = $this->file_dir . basename($_FILES[$upload_key]["name"]);
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        if (isset($this->config['file_type'])) {
            $status = false;
            foreach ($this->config['file_type'] as $item) {
                if ($item == $imageFileType) $status = true;
            }
        }
        else $status = true;
        return $status;
    }

    // Do Upload
    public function do_upload($upload_key='foo') {
        if (!isset($_FILES[$upload_key]['name'])) 
            return array(
                'file_name' => null,
                'file_location' => null,
                'status' => false
            );

        $fileName = date("YmdHis") . $_FILES[$upload_key]['name'];
        $fileDir = $this->file_dir . $fileName;
        $file = $_FILES[$upload_key]['tmp_name'];
        $res = array();

        $res['file_name'] = $fileName;
        $res['file_location'] = $fileDir;
        $res['status'] = false;

        if ($this->checkSize($upload_key) && $this->checkFileType($upload_key)) {
            if (move_uploaded_file($file, $fileDir)) {
                $res['file_name'] = $fileName;
                $res['file_location'] = $fileDir;
                $res['status'] = true;
            }
        }

        return $res;
    }
}