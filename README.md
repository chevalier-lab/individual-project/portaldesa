# PortalDesa
Merupakan aplikasi pemberitaan dari Desa Netpala yang ada di Nusa Tenggara Timur.

## Deskripsi Fitur
Aplikasi ini dibangun dengan fitur sebagai berikut:
1. Landing Page
2. Dashboard
3. Manajemen Berita
4. Manajemen Halaman
5. Manajemen Menu
6. Manajemen Pengguna

## Deskripsi Teknologi
Aplikasi ini dibangun dengan mengggunakan:
1. Codeigniter
2. AdminLTE Template
3. MySQL
4. HMVC

## Copyright
Aplikasi ini dikhususkan untuk desa netpala, jika terdapat duplikasi dan publikasi yang tidak melalui izin pemerintah setempat, maka akan dikenakan sanksi hukum.