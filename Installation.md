# How To Install
1. Download / Clone this repository using command git clone [repository http url]
```git
git clone https://gitlab.com/chevalier-lab/individual-project/portaldesa.git
```

2. Don't forget to do the command above in htdocs folder for ex: <portaldesa>
3. Running your apache + mysql (recomended using XAMPP)
4. Open your browser and go to [localhost/phpmyadmin](localhost/phpmyadmin)
5. Now go to the SQL Command and Execute all sql file in databases folders on this projects.
6. Don't forget to set-up the base-url on folder [application/config/config.php](application/config/config.php), change it with [localhost/yourFolderProject](localhost/yourFolderProject) for ex: [localhost/portaldesa](localhost/portaldesa)
7. Now running the seeder / default data with using url : [localhost/yourFolderProject/index.php/controller/installation](localhost/yourFolderProject/index.php/controller/installation) for ex: [localhost/portaldesa/index.php/controller/installation](localhost/portaldesa/index.php/controller/installation)
8. Yey your application is now ready to develop. Good Job!!.